/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosImpl;

import com.ec.ServiciosLocal.InformaticaFacadeLocal;
import com.ec.Entidades.Informatica;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Xavier
 */
@Stateless
public class InformaticaFacade extends AbstractFacade<Informatica> implements InformaticaFacadeLocal {
    @PersistenceContext(unitName = "AZCA_TrabajosPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public InformaticaFacade() {
        super(Informatica.class);
    }
    
}

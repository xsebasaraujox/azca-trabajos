/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosImpl;

import com.ec.ServiciosLocal.AreaFacadeLocal;
import com.ec.Entidades.Area;
import com.ec.Entidades.Subarea;
import java.util.List;
import javax.ejb.Stateless;
import javax.management.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Xavier
 */
@Stateless
public class AreaFacade extends AbstractFacade<Area> implements AreaFacadeLocal {
    @PersistenceContext(unitName = "AZCA_TrabajosPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AreaFacade() {
        super(Area.class);
    }
    
    @Override
    public Area obtenerAreaPorID(Integer id){
        javax.persistence.Query query = em.createNamedQuery("Area.findByArId");
        query.setParameter("arId", id);
        return (Area) query.getSingleResult();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosImpl;

import com.ec.ServiciosLocal.DetalleCatalogoFacadeLocal;
import com.ec.Entidades.DetalleCatalogo;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Xavier
 */
@Stateless
public class DetalleCatalogoFacade extends AbstractFacade<DetalleCatalogo> implements DetalleCatalogoFacadeLocal {
    @PersistenceContext(unitName = "AZCA_TrabajosPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DetalleCatalogoFacade() {
        super(DetalleCatalogo.class);
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosImpl;

import com.ec.Entidades.Area;
import com.ec.ServiciosLocal.SubareaFacadeLocal;
import com.ec.Entidades.Subarea;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Xavier
 */
@Stateless
public class SubareaFacade extends AbstractFacade<Subarea> implements SubareaFacadeLocal {
    @PersistenceContext(unitName = "AZCA_TrabajosPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SubareaFacade() {
        super(Subarea.class);
    }
    
    @Override
    public List<Subarea> obtenerSubareasPorArea(Area area){
        Query query = em.createNamedQuery("Subarea.findByArea");
        query.setParameter("arId", area.getArId());
        return query.getResultList();
    }
    
    @Override
    public Subarea obtenerSubareasPorId(Integer id){
        Query query = em.createNamedQuery("Subarea.findBySaId");
        query.setParameter("saId", id);
        return (Subarea) query.getSingleResult();
    }
    
}

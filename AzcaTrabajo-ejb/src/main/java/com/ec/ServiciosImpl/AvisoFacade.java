/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosImpl;

import com.ec.ServiciosLocal.AvisoFacadeLocal;
import com.ec.Entidades.Aviso;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Xavier
 */
@Stateless
public class AvisoFacade extends AbstractFacade<Aviso> implements AvisoFacadeLocal {
    @PersistenceContext(unitName = "AZCA_TrabajosPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AvisoFacade() {
        super(Aviso.class);
    }
    
}

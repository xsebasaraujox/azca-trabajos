/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosImpl;

import com.ec.ServiciosLocal.DetalleCatalogoTipoFacadeLocal;
import com.ec.Entidades.DetalleCatalogoTipo;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Xavier
 */
@Stateless
public class DetalleCatalogoTipoFacade extends AbstractFacade<DetalleCatalogoTipo> implements DetalleCatalogoTipoFacadeLocal {
    @PersistenceContext(unitName = "AZCA_TrabajosPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DetalleCatalogoTipoFacade() {
        super(DetalleCatalogoTipo.class);
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosImpl;

import com.ec.ServiciosLocal.DatosEmpresaFacadeLocal;
import com.ec.Entidades.DatosEmpresa;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Xavier
 */
@Stateless
public class DatosEmpresaFacade extends AbstractFacade<DatosEmpresa> implements DatosEmpresaFacadeLocal {
    @PersistenceContext(unitName = "AZCA_TrabajosPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DatosEmpresaFacade() {
        super(DatosEmpresa.class);
    }
    
}

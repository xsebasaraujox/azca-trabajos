/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosImpl;

import com.ec.ServiciosLocal.ConocimientoAdicionalFacadeLocal;
import com.ec.Entidades.ConocimientoAdicional;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Xavier
 */
@Stateless
public class ConocimientoAdicionalFacade extends AbstractFacade<ConocimientoAdicional> implements ConocimientoAdicionalFacadeLocal {
    @PersistenceContext(unitName = "AZCA_TrabajosPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ConocimientoAdicionalFacade() {
        super(ConocimientoAdicional.class);
    }
    
}

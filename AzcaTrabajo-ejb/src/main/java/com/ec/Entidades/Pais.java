/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sebastian
 */
@Entity
@Table(name = "pais")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pais.findAll", query = "SELECT p FROM Pais p"),
    @NamedQuery(name = "Pais.findByPaId", query = "SELECT p FROM Pais p WHERE p.paId = :paId"),
    @NamedQuery(name = "Pais.findByPaDescripcion", query = "SELECT p FROM Pais p WHERE p.paDescripcion = :paDescripcion")})
public class Pais implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PA_ID")
    private Integer paId;
    @Size(max = 50)
    @Column(name = "PA_DESCRIPCION")
    private String paDescripcion;
    @OneToMany(mappedBy = "paId")
    private List<Provincia> provinciaList;
    @OneToMany(mappedBy = "paId")
    private List<ExperienciaLaboral> experienciaLaboralList;
    @OneToMany(mappedBy = "paId")
    private List<Estudios> estudiosList;

    public Pais() {
    }

    public Pais(Integer paId) {
        this.paId = paId;
    }

    public Integer getPaId() {
        return paId;
    }

    public void setPaId(Integer paId) {
        this.paId = paId;
    }

    public String getPaDescripcion() {
        return paDescripcion;
    }

    public void setPaDescripcion(String paDescripcion) {
        this.paDescripcion = paDescripcion;
    }

    @XmlTransient
    public List<Provincia> getProvinciaList() {
        return provinciaList;
    }

    public void setProvinciaList(List<Provincia> provinciaList) {
        this.provinciaList = provinciaList;
    }

    @XmlTransient
    public List<ExperienciaLaboral> getExperienciaLaboralList() {
        return experienciaLaboralList;
    }

    public void setExperienciaLaboralList(List<ExperienciaLaboral> experienciaLaboralList) {
        this.experienciaLaboralList = experienciaLaboralList;
    }

    @XmlTransient
    public List<Estudios> getEstudiosList() {
        return estudiosList;
    }

    public void setEstudiosList(List<Estudios> estudiosList) {
        this.estudiosList = estudiosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (paId != null ? paId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pais)) {
            return false;
        }
        Pais other = (Pais) object;
        if ((this.paId == null && other.paId != null) || (this.paId != null && !this.paId.equals(other.paId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ec.Entidades.Pais[ paId=" + paId + " ]";
    }
    
}

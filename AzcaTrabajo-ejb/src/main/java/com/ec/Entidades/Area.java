/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Entidades;

import com.ec.Interfaces.Imprimible;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sebastian
 */
@Entity
@Table(name = "area")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Area.findAll", query = "SELECT a FROM Area a"),
    @NamedQuery(name = "Area.findByArId", query = "SELECT a FROM Area a WHERE a.arId = :arId"),
    @NamedQuery(name = "Area.findByArDescripcion", query = "SELECT a FROM Area a WHERE a.arDescripcion = :arDescripcion")})
public class Area implements Serializable, Imprimible {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "AR_ID")
    private Integer arId;
    @Size(max = 50)
    @Column(name = "AR_DESCRIPCION")
    private String arDescripcion;
    @OneToMany(mappedBy = "arId")
    private List<Subarea> subareaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "arId")
    private List<Informatica> informaticaList;
    @OneToMany(mappedBy = "arId")
    private List<ExperienciaLaboral> experienciaLaboralList;
    @OneToMany(mappedBy = "arId")
    private List<Estudios> estudiosList;
    @OneToMany(mappedBy = "arId")
    private List<Aviso> avisoList;

    public Area() {
    }

    public Area(Integer arId) {
        this.arId = arId;
    }
    
    public Area(Integer arId, String arDescripcion){
        this.arId = arId;
        this.arDescripcion = arDescripcion;
    }

    public Integer getArId() {
        return arId;
    }

    public void setArId(Integer arId) {
        this.arId = arId;
    }

    public String getArDescripcion() {
        return arDescripcion;
    }

    public void setArDescripcion(String arDescripcion) {
        this.arDescripcion = arDescripcion;
    }

    @XmlTransient
    public List<Subarea> getSubareaList() {
        return subareaList;
    }

    public void setSubareaList(List<Subarea> subareaList) {
        this.subareaList = subareaList;
    }

    @XmlTransient
    public List<Informatica> getInformaticaList() {
        return informaticaList;
    }

    public void setInformaticaList(List<Informatica> informaticaList) {
        this.informaticaList = informaticaList;
    }

    @XmlTransient
    public List<ExperienciaLaboral> getExperienciaLaboralList() {
        return experienciaLaboralList;
    }

    public void setExperienciaLaboralList(List<ExperienciaLaboral> experienciaLaboralList) {
        this.experienciaLaboralList = experienciaLaboralList;
    }

    @XmlTransient
    public List<Estudios> getEstudiosList() {
        return estudiosList;
    }

    public void setEstudiosList(List<Estudios> estudiosList) {
        this.estudiosList = estudiosList;
    }

    @XmlTransient
    public List<Aviso> getAvisoList() {
        return avisoList;
    }

    public void setAvisoList(List<Aviso> avisoList) {
        this.avisoList = avisoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (arId != null ? arId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Area)) {
            return false;
        }
        Area other = (Area) object;
        if ((this.arId == null && other.arId != null) || (this.arId != null && !this.arId.equals(other.arId))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {        
        return "com.ec.Entidades.Area[ arId=" + arId + " ]";
    }
    
    @Override
    public String getEtiqueta() {
        return getArDescripcion();
    }

    @Override
    public Object getObjecto() {
        return this;
    }
    
}

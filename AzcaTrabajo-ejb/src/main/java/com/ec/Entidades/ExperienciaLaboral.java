/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sebastian
 */
@Entity
@Table(name = "experiencia_laboral")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExperienciaLaboral.findAll", query = "SELECT e FROM ExperienciaLaboral e"),
    @NamedQuery(name = "ExperienciaLaboral.findByElId", query = "SELECT e FROM ExperienciaLaboral e WHERE e.elId = :elId"),
    @NamedQuery(name = "ExperienciaLaboral.findByElTitulo", query = "SELECT e FROM ExperienciaLaboral e WHERE e.elTitulo = :elTitulo"),
    @NamedQuery(name = "ExperienciaLaboral.findByElFechaInicio", query = "SELECT e FROM ExperienciaLaboral e WHERE e.elFechaInicio = :elFechaInicio"),
    @NamedQuery(name = "ExperienciaLaboral.findByElEmpresa", query = "SELECT e FROM ExperienciaLaboral e WHERE e.elEmpresa = :elEmpresa"),
    @NamedQuery(name = "ExperienciaLaboral.findByElFechaFin", query = "SELECT e FROM ExperienciaLaboral e WHERE e.elFechaFin = :elFechaFin"),
    @NamedQuery(name = "ExperienciaLaboral.findByElDescripcion", query = "SELECT e FROM ExperienciaLaboral e WHERE e.elDescripcion = :elDescripcion")})
public class ExperienciaLaboral implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "EL_ID")
    private Integer elId;
    @Size(max = 50)
    @Column(name = "EL_TITULO")
    private String elTitulo;
    @Column(name = "EL_FECHA_INICIO")
    @Temporal(TemporalType.DATE)
    private Date elFechaInicio;
    @Size(max = 50)
    @Column(name = "EL_EMPRESA")
    private String elEmpresa;
    @Column(name = "EL_FECHA_FIN")
    @Temporal(TemporalType.DATE)
    private Date elFechaFin;
    @Size(max = 50)
    @Column(name = "EL_DESCRIPCION")
    private String elDescripcion;
    @OneToMany(mappedBy = "elId")
    private List<Referencias> referenciasList;
    @JoinColumn(name = "SENORITY", referencedColumnName = "DC_ID")
    @ManyToOne
    private DetalleCatalogo senority;
    @JoinColumn(name = "POS_ID", referencedColumnName = "POS_ID")
    @ManyToOne
    private Postulante posId;
    @JoinColumn(name = "PA_ID", referencedColumnName = "PA_ID")
    @ManyToOne
    private Pais paId;
    @JoinColumn(name = "INDUSTRIA", referencedColumnName = "DC_ID")
    @ManyToOne
    private DetalleCatalogo industria;
    @JoinColumn(name = "AR_ID", referencedColumnName = "AR_ID")
    @ManyToOne
    private Area arId;
    @JoinColumn(name = "SA_ID", referencedColumnName = "SA_ID")
    @ManyToOne
    private Subarea saId;

    public ExperienciaLaboral() {
    }

    public ExperienciaLaboral(Integer elId) {
        this.elId = elId;
    }

    public Integer getElId() {
        return elId;
    }

    public void setElId(Integer elId) {
        this.elId = elId;
    }

    public String getElTitulo() {
        return elTitulo;
    }

    public void setElTitulo(String elTitulo) {
        this.elTitulo = elTitulo;
    }

    public Date getElFechaInicio() {
        return elFechaInicio;
    }

    public void setElFechaInicio(Date elFechaInicio) {
        this.elFechaInicio = elFechaInicio;
    }

    public String getElEmpresa() {
        return elEmpresa;
    }

    public void setElEmpresa(String elEmpresa) {
        this.elEmpresa = elEmpresa;
    }

    public Date getElFechaFin() {
        return elFechaFin;
    }

    public void setElFechaFin(Date elFechaFin) {
        this.elFechaFin = elFechaFin;
    }

    public String getElDescripcion() {
        return elDescripcion;
    }

    public void setElDescripcion(String elDescripcion) {
        this.elDescripcion = elDescripcion;
    }

    @XmlTransient
    public List<Referencias> getReferenciasList() {
        return referenciasList;
    }

    public void setReferenciasList(List<Referencias> referenciasList) {
        this.referenciasList = referenciasList;
    }

    public DetalleCatalogo getSenority() {
        return senority;
    }

    public void setSenority(DetalleCatalogo senority) {
        this.senority = senority;
    }

    public Postulante getPosId() {
        return posId;
    }

    public void setPosId(Postulante posId) {
        this.posId = posId;
    }

    public Pais getPaId() {
        return paId;
    }

    public void setPaId(Pais paId) {
        this.paId = paId;
    }

    public DetalleCatalogo getIndustria() {
        return industria;
    }

    public void setIndustria(DetalleCatalogo industria) {
        this.industria = industria;
    }

    public Area getArId() {
        return arId;
    }

    public void setArId(Area arId) {
        this.arId = arId;
    }

    public Subarea getSaId() {
        return saId;
    }

    public void setSaId(Subarea saId) {
        this.saId = saId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (elId != null ? elId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExperienciaLaboral)) {
            return false;
        }
        ExperienciaLaboral other = (ExperienciaLaboral) object;
        if ((this.elId == null && other.elId != null) || (this.elId != null && !this.elId.equals(other.elId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ec.Entidades.ExperienciaLaboral[ elId=" + elId + " ]";
    }
    
}

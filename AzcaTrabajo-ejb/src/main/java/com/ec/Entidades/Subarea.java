/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Entidades;

import com.ec.Interfaces.Imprimible;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sebastian
 */
@Entity
@Table(name = "subarea")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Subarea.findAll", query = "SELECT s FROM Subarea s"),
    @NamedQuery(name = "Subarea.findBySaId", query = "SELECT s FROM Subarea s WHERE s.saId = :saId"),
    @NamedQuery(name = "Subarea.findBySaDescripcion", query = "SELECT s FROM Subarea s WHERE s.saDescripcion = :saDescripcion"),
    @NamedQuery(name = "Subarea.findByArea", query = "SELECT s FROM Subarea s WHERE s.arId.arId = :arId")})
public class Subarea implements Serializable, Imprimible {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "SA_ID")
    private Integer saId;
    @Size(max = 50)
    @Column(name = "SA_DESCRIPCION")
    private String saDescripcion;
    @JoinColumn(name = "AR_ID", referencedColumnName = "AR_ID")
    @ManyToOne
    private Area arId;
    @OneToMany(mappedBy = "saId")
    private List<Informatica> informaticaList;
    @OneToMany(mappedBy = "saId")
    private List<ExperienciaLaboral> experienciaLaboralList;
    @OneToMany(mappedBy = "saId")
    private List<Aviso> avisoList;

    public Subarea() {
    }

    public Subarea(Integer saId) {
        this.saId = saId;
    }

    public Integer getSaId() {
        return saId;
    }

    public void setSaId(Integer saId) {
        this.saId = saId;
    }

    public String getSaDescripcion() {
        return saDescripcion;
    }

    public void setSaDescripcion(String saDescripcion) {
        this.saDescripcion = saDescripcion;
    }

    public Area getArId() {
        return arId;
    }

    public void setArId(Area arId) {
        this.arId = arId;
    }

    @XmlTransient
    public List<Informatica> getInformaticaList() {
        return informaticaList;
    }

    public void setInformaticaList(List<Informatica> informaticaList) {
        this.informaticaList = informaticaList;
    }

    @XmlTransient
    public List<ExperienciaLaboral> getExperienciaLaboralList() {
        return experienciaLaboralList;
    }

    public void setExperienciaLaboralList(List<ExperienciaLaboral> experienciaLaboralList) {
        this.experienciaLaboralList = experienciaLaboralList;
    }

    @XmlTransient
    public List<Aviso> getAvisoList() {
        return avisoList;
    }

    public void setAvisoList(List<Aviso> avisoList) {
        this.avisoList = avisoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (saId != null ? saId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Subarea)) {
            return false;
        }
        Subarea other = (Subarea) object;
        if ((this.saId == null && other.saId != null) || (this.saId != null && !this.saId.equals(other.saId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ec.Entidades.Subarea[ saId=" + saId + " ]";
    }

    @Override
    public String getEtiqueta() {
        return getSaDescripcion();
    }

    @Override
    public Object getObjecto() {
        return this;
    }
}

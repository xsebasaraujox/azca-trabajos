/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sebastian
 */
@Entity
@Table(name = "datos_empresa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DatosEmpresa.findAll", query = "SELECT d FROM DatosEmpresa d"),
    @NamedQuery(name = "DatosEmpresa.findByDeId", query = "SELECT d FROM DatosEmpresa d WHERE d.deId = :deId"),
    @NamedQuery(name = "DatosEmpresa.findByDeNombre", query = "SELECT d FROM DatosEmpresa d WHERE d.deNombre = :deNombre"),
    @NamedQuery(name = "DatosEmpresa.findByDeRuc", query = "SELECT d FROM DatosEmpresa d WHERE d.deRuc = :deRuc"),
    @NamedQuery(name = "DatosEmpresa.findByDeTelefono", query = "SELECT d FROM DatosEmpresa d WHERE d.deTelefono = :deTelefono"),
    @NamedQuery(name = "DatosEmpresa.findByDeCorreo", query = "SELECT d FROM DatosEmpresa d WHERE d.deCorreo = :deCorreo")})
public class DatosEmpresa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DE_ID")
    private Integer deId;
    @Size(max = 50)
    @Column(name = "DE_NOMBRE")
    private String deNombre;
    @Size(max = 13)
    @Column(name = "DE_RUC")
    private String deRuc;
    @Size(max = 10)
    @Column(name = "DE_TELEFONO")
    private String deTelefono;
    @Size(max = 50)
    @Column(name = "DE_CORREO")
    private String deCorreo;
    @OneToMany(mappedBy = "deId")
    private List<Empresa> empresaList;

    public DatosEmpresa() {
    }

    public DatosEmpresa(Integer deId) {
        this.deId = deId;
    }

    public Integer getDeId() {
        return deId;
    }

    public void setDeId(Integer deId) {
        this.deId = deId;
    }

    public String getDeNombre() {
        return deNombre;
    }

    public void setDeNombre(String deNombre) {
        this.deNombre = deNombre;
    }

    public String getDeRuc() {
        return deRuc;
    }

    public void setDeRuc(String deRuc) {
        this.deRuc = deRuc;
    }

    public String getDeTelefono() {
        return deTelefono;
    }

    public void setDeTelefono(String deTelefono) {
        this.deTelefono = deTelefono;
    }

    public String getDeCorreo() {
        return deCorreo;
    }

    public void setDeCorreo(String deCorreo) {
        this.deCorreo = deCorreo;
    }

    @XmlTransient
    public List<Empresa> getEmpresaList() {
        return empresaList;
    }

    public void setEmpresaList(List<Empresa> empresaList) {
        this.empresaList = empresaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (deId != null ? deId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatosEmpresa)) {
            return false;
        }
        DatosEmpresa other = (DatosEmpresa) object;
        if ((this.deId == null && other.deId != null) || (this.deId != null && !this.deId.equals(other.deId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ec.Entidades.DatosEmpresa[ deId=" + deId + " ]";
    }
    
}

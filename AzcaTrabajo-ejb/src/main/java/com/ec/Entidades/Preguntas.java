/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Xavier
 */
@Entity
@Table(name = "preguntas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Preguntas.findAll", query = "SELECT p FROM Preguntas p"),
    @NamedQuery(name = "Preguntas.findByPrgId", query = "SELECT p FROM Preguntas p WHERE p.prgId = :prgId"),
    @NamedQuery(name = "Preguntas.findByPrgRespuesta", query = "SELECT p FROM Preguntas p WHERE p.prgRespuesta = :prgRespuesta"),
    @NamedQuery(name = "Preguntas.findByPrgTitulo", query = "SELECT p FROM Preguntas p WHERE p.prgTitulo = :prgTitulo")})
public class Preguntas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRG_ID")
    private Integer prgId;
    @Size(max = 100)
    @Column(name = "PRG_RESPUESTA")
    private String prgRespuesta;
    @Size(max = 50)
    @Column(name = "PRG_TITULO")
    private String prgTitulo;
    @JoinColumn(name = "AV_ID", referencedColumnName = "AV_ID")
    @ManyToOne
    private Aviso avId;

    public Preguntas() {
    }

    public Preguntas(Integer prgId) {
        this.prgId = prgId;
    }

    public Integer getPrgId() {
        return prgId;
    }

    public void setPrgId(Integer prgId) {
        this.prgId = prgId;
    }

    public String getPrgRespuesta() {
        return prgRespuesta;
    }

    public void setPrgRespuesta(String prgRespuesta) {
        this.prgRespuesta = prgRespuesta;
    }

    public String getPrgTitulo() {
        return prgTitulo;
    }

    public void setPrgTitulo(String prgTitulo) {
        this.prgTitulo = prgTitulo;
    }

    public Aviso getAvId() {
        return avId;
    }

    public void setAvId(Aviso avId) {
        this.avId = avId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (prgId != null ? prgId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Preguntas)) {
            return false;
        }
        Preguntas other = (Preguntas) object;
        if ((this.prgId == null && other.prgId != null) || (this.prgId != null && !this.prgId.equals(other.prgId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ec.Entidades.Preguntas[ prgId=" + prgId + " ]";
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sebastian
 */
@Entity
@Table(name = "referencias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Referencias.findAll", query = "SELECT r FROM Referencias r"),
    @NamedQuery(name = "Referencias.findByReId", query = "SELECT r FROM Referencias r WHERE r.reId = :reId"),
    @NamedQuery(name = "Referencias.findByReNombre", query = "SELECT r FROM Referencias r WHERE r.reNombre = :reNombre"),
    @NamedQuery(name = "Referencias.findByReApellido", query = "SELECT r FROM Referencias r WHERE r.reApellido = :reApellido"),
    @NamedQuery(name = "Referencias.findByReCorreo", query = "SELECT r FROM Referencias r WHERE r.reCorreo = :reCorreo"),
    @NamedQuery(name = "Referencias.findByReTelefono", query = "SELECT r FROM Referencias r WHERE r.reTelefono = :reTelefono"),
    @NamedQuery(name = "Referencias.findByRePuesto", query = "SELECT r FROM Referencias r WHERE r.rePuesto = :rePuesto")})
public class Referencias implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "RE_ID")
    private Integer reId;
    @Size(max = 50)
    @Column(name = "RE_NOMBRE")
    private String reNombre;
    @Size(max = 50)
    @Column(name = "RE_APELLIDO")
    private String reApellido;
    @Size(max = 50)
    @Column(name = "RE_CORREO")
    private String reCorreo;
    @Size(max = 10)
    @Column(name = "RE_TELEFONO")
    private String reTelefono;
    @Size(max = 50)
    @Column(name = "RE_PUESTO")
    private String rePuesto;
    @JoinColumn(name = "TIPO", referencedColumnName = "DC_ID")
    @ManyToOne
    private DetalleCatalogo tipo;
    @JoinColumn(name = "POS_ID", referencedColumnName = "POS_ID")
    @ManyToOne
    private Postulante posId;
    @JoinColumn(name = "EL_ID", referencedColumnName = "EL_ID")
    @ManyToOne
    private ExperienciaLaboral elId;
    @JoinColumn(name = "RELACION", referencedColumnName = "DC_ID")
    @ManyToOne
    private DetalleCatalogo relacion;

    public Referencias() {
    }

    public Referencias(Integer reId) {
        this.reId = reId;
    }

    public Integer getReId() {
        return reId;
    }

    public void setReId(Integer reId) {
        this.reId = reId;
    }

    public String getReNombre() {
        return reNombre;
    }

    public void setReNombre(String reNombre) {
        this.reNombre = reNombre;
    }

    public String getReApellido() {
        return reApellido;
    }

    public void setReApellido(String reApellido) {
        this.reApellido = reApellido;
    }

    public String getReCorreo() {
        return reCorreo;
    }

    public void setReCorreo(String reCorreo) {
        this.reCorreo = reCorreo;
    }

    public String getReTelefono() {
        return reTelefono;
    }

    public void setReTelefono(String reTelefono) {
        this.reTelefono = reTelefono;
    }

    public String getRePuesto() {
        return rePuesto;
    }

    public void setRePuesto(String rePuesto) {
        this.rePuesto = rePuesto;
    }

    public DetalleCatalogo getTipo() {
        return tipo;
    }

    public void setTipo(DetalleCatalogo tipo) {
        this.tipo = tipo;
    }

    public Postulante getPosId() {
        return posId;
    }

    public void setPosId(Postulante posId) {
        this.posId = posId;
    }

    public ExperienciaLaboral getElId() {
        return elId;
    }

    public void setElId(ExperienciaLaboral elId) {
        this.elId = elId;
    }

    public DetalleCatalogo getRelacion() {
        return relacion;
    }

    public void setRelacion(DetalleCatalogo relacion) {
        this.relacion = relacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reId != null ? reId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Referencias)) {
            return false;
        }
        Referencias other = (Referencias) object;
        if ((this.reId == null && other.reId != null) || (this.reId != null && !this.reId.equals(other.reId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ec.Entidades.Referencias[ reId=" + reId + " ]";
    }
    
}

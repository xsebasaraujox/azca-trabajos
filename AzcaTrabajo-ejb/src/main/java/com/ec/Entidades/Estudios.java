/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sebastian
 */
@Entity
@Table(name = "estudios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Estudios.findAll", query = "SELECT e FROM Estudios e"),
    @NamedQuery(name = "Estudios.findByEsId", query = "SELECT e FROM Estudios e WHERE e.esId = :esId"),
    @NamedQuery(name = "Estudios.findByEsTitulo", query = "SELECT e FROM Estudios e WHERE e.esTitulo = :esTitulo"),
    @NamedQuery(name = "Estudios.findByEsFehcaInicio", query = "SELECT e FROM Estudios e WHERE e.esFehcaInicio = :esFehcaInicio"),
    @NamedQuery(name = "Estudios.findByEsFechaFin", query = "SELECT e FROM Estudios e WHERE e.esFechaFin = :esFechaFin"),
    @NamedQuery(name = "Estudios.findByEsPorcentaje", query = "SELECT e FROM Estudios e WHERE e.esPorcentaje = :esPorcentaje")})
public class Estudios implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ES_ID")
    private Integer esId;
    @Size(max = 50)
    @Column(name = "ES_TITULO")
    private String esTitulo;
    @Column(name = "ES_FEHCA_INICIO")
    @Temporal(TemporalType.DATE)
    private Date esFehcaInicio;
    @Column(name = "ES_FECHA_FIN")
    @Temporal(TemporalType.DATE)
    private Date esFechaFin;
    @Column(name = "ES_PORCENTAJE")
    private Integer esPorcentaje;
    @JoinColumn(name = "PA_ID", referencedColumnName = "PA_ID")
    @ManyToOne
    private Pais paId;
    @JoinColumn(name = "NIVEL", referencedColumnName = "DC_ID")
    @ManyToOne
    private DetalleCatalogo nivel;
    @JoinColumn(name = "ESTADO", referencedColumnName = "DC_ID")
    @ManyToOne
    private DetalleCatalogo estado;
    @JoinColumn(name = "INSTITUCION", referencedColumnName = "DC_ID")
    @ManyToOne
    private DetalleCatalogo institucion;
    @JoinColumn(name = "AR_ID", referencedColumnName = "AR_ID")
    @ManyToOne
    private Area arId;
    @JoinColumn(name = "POS_ID", referencedColumnName = "POS_ID")
    @ManyToOne
    private Postulante posId;

    public Estudios() {
    }

    public Estudios(Integer esId) {
        this.esId = esId;
    }

    public Integer getEsId() {
        return esId;
    }

    public void setEsId(Integer esId) {
        this.esId = esId;
    }

    public String getEsTitulo() {
        return esTitulo;
    }

    public void setEsTitulo(String esTitulo) {
        this.esTitulo = esTitulo;
    }

    public Date getEsFehcaInicio() {
        return esFehcaInicio;
    }

    public void setEsFehcaInicio(Date esFehcaInicio) {
        this.esFehcaInicio = esFehcaInicio;
    }

    public Date getEsFechaFin() {
        return esFechaFin;
    }

    public void setEsFechaFin(Date esFechaFin) {
        this.esFechaFin = esFechaFin;
    }

    public Integer getEsPorcentaje() {
        return esPorcentaje;
    }

    public void setEsPorcentaje(Integer esPorcentaje) {
        this.esPorcentaje = esPorcentaje;
    }

    public Pais getPaId() {
        return paId;
    }

    public void setPaId(Pais paId) {
        this.paId = paId;
    }

    public DetalleCatalogo getNivel() {
        return nivel;
    }

    public void setNivel(DetalleCatalogo nivel) {
        this.nivel = nivel;
    }

    public DetalleCatalogo getEstado() {
        return estado;
    }

    public void setEstado(DetalleCatalogo estado) {
        this.estado = estado;
    }

    public DetalleCatalogo getInstitucion() {
        return institucion;
    }

    public void setInstitucion(DetalleCatalogo institucion) {
        this.institucion = institucion;
    }

    public Area getArId() {
        return arId;
    }

    public void setArId(Area arId) {
        this.arId = arId;
    }

    public Postulante getPosId() {
        return posId;
    }

    public void setPosId(Postulante posId) {
        this.posId = posId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (esId != null ? esId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estudios)) {
            return false;
        }
        Estudios other = (Estudios) object;
        if ((this.esId == null && other.esId != null) || (this.esId != null && !this.esId.equals(other.esId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ec.Entidades.Estudios[ esId=" + esId + " ]";
    }
    
}

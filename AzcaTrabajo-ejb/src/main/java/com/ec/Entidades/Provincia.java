/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Entidades;

import com.ec.Interfaces.Imprimible;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sebastian
 */
@Entity
@Table(name = "provincia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Provincia.findAll", query = "SELECT p FROM Provincia p order by p.proDescripcion"),
    @NamedQuery(name = "Provincia.findByProId", query = "SELECT p FROM Provincia p WHERE p.proId = :proId"),
    @NamedQuery(name = "Provincia.findByProDescripcion", query = "SELECT p FROM Provincia p WHERE p.proDescripcion = :proDescripcion")})
public class Provincia implements Serializable, Imprimible {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRO_ID")
    private Integer proId;
    @Size(max = 50)
    @Column(name = "PRO_DESCRIPCION")
    private String proDescripcion;
    @JoinColumn(name = "PA_ID", referencedColumnName = "PA_ID")
    @ManyToOne
    private Pais paId;
    @OneToMany(mappedBy = "proId")
    private List<Aviso> avisoList;

    public Provincia() {
    }

    public Provincia(Integer proId) {
        this.proId = proId;
    }

    public Integer getProId() {
        return proId;
    }

    public void setProId(Integer proId) {
        this.proId = proId;
    }

    public String getProDescripcion() {
        return proDescripcion;
    }

    public void setProDescripcion(String proDescripcion) {
        this.proDescripcion = proDescripcion;
    }

    public Pais getPaId() {
        return paId;
    }

    public void setPaId(Pais paId) {
        this.paId = paId;
    }

    @XmlTransient
    public List<Aviso> getAvisoList() {
        return avisoList;
    }

    public void setAvisoList(List<Aviso> avisoList) {
        this.avisoList = avisoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proId != null ? proId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Provincia)) {
            return false;
        }
        Provincia other = (Provincia) object;
        if ((this.proId == null && other.proId != null) || (this.proId != null && !this.proId.equals(other.proId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ec.Entidades.Provincia[ proId=" + proId + " ]";
    }
    
    @Override
    public String getEtiqueta() {
        return getProDescripcion();
    }

    @Override
    public Object getObjecto() {
        return this;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sebastian
 */
@Entity
@Table(name = "idiomas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Idiomas.findAll", query = "SELECT i FROM Idiomas i"),
    @NamedQuery(name = "Idiomas.findByIdId", query = "SELECT i FROM Idiomas i WHERE i.idId = :idId")})
public class Idiomas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ID")
    private Integer idId;
    @JoinColumn(name = "IDIOMA", referencedColumnName = "DC_ID")
    @ManyToOne
    private DetalleCatalogo idioma;
    @JoinColumn(name = "ESCRITO_NIVEL", referencedColumnName = "DC_ID")
    @ManyToOne
    private DetalleCatalogo escritoNivel;
    @JoinColumn(name = "ORAL_NIVEL", referencedColumnName = "DC_ID")
    @ManyToOne
    private DetalleCatalogo oralNivel;
    @JoinColumn(name = "POS_ID", referencedColumnName = "POS_ID")
    @ManyToOne
    private Postulante posId;

    public Idiomas() {
    }

    public Idiomas(Integer idId) {
        this.idId = idId;
    }

    public Integer getIdId() {
        return idId;
    }

    public void setIdId(Integer idId) {
        this.idId = idId;
    }

    public DetalleCatalogo getIdioma() {
        return idioma;
    }

    public void setIdioma(DetalleCatalogo idioma) {
        this.idioma = idioma;
    }

    public DetalleCatalogo getEscritoNivel() {
        return escritoNivel;
    }

    public void setEscritoNivel(DetalleCatalogo escritoNivel) {
        this.escritoNivel = escritoNivel;
    }

    public DetalleCatalogo getOralNivel() {
        return oralNivel;
    }

    public void setOralNivel(DetalleCatalogo oralNivel) {
        this.oralNivel = oralNivel;
    }

    public Postulante getPosId() {
        return posId;
    }

    public void setPosId(Postulante posId) {
        this.posId = posId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idId != null ? idId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Idiomas)) {
            return false;
        }
        Idiomas other = (Idiomas) object;
        if ((this.idId == null && other.idId != null) || (this.idId != null && !this.idId.equals(other.idId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ec.Entidades.Idiomas[ idId=" + idId + " ]";
    }
    
}

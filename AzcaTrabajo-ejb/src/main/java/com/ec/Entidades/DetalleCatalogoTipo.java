/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sebastian
 */
@Entity
@Table(name = "detalle_catalogo_tipo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalleCatalogoTipo.findAll", query = "SELECT d FROM DetalleCatalogoTipo d"),
    @NamedQuery(name = "DetalleCatalogoTipo.findByDctId", query = "SELECT d FROM DetalleCatalogoTipo d WHERE d.dctId = :dctId"),
    @NamedQuery(name = "DetalleCatalogoTipo.findByDctDescripcion", query = "SELECT d FROM DetalleCatalogoTipo d WHERE d.dctDescripcion = :dctDescripcion")})
public class DetalleCatalogoTipo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DCT_ID")
    private Integer dctId;
    @Size(max = 50)
    @Column(name = "DCT_DESCRIPCION")
    private String dctDescripcion;
    @OneToMany(mappedBy = "dctId")
    private List<DetalleCatalogo> detalleCatalogoList;

    public DetalleCatalogoTipo() {
    }

    public DetalleCatalogoTipo(Integer dctId) {
        this.dctId = dctId;
    }

    public Integer getDctId() {
        return dctId;
    }

    public void setDctId(Integer dctId) {
        this.dctId = dctId;
    }

    public String getDctDescripcion() {
        return dctDescripcion;
    }

    public void setDctDescripcion(String dctDescripcion) {
        this.dctDescripcion = dctDescripcion;
    }

    @XmlTransient
    public List<DetalleCatalogo> getDetalleCatalogoList() {
        return detalleCatalogoList;
    }

    public void setDetalleCatalogoList(List<DetalleCatalogo> detalleCatalogoList) {
        this.detalleCatalogoList = detalleCatalogoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dctId != null ? dctId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleCatalogoTipo)) {
            return false;
        }
        DetalleCatalogoTipo other = (DetalleCatalogoTipo) object;
        if ((this.dctId == null && other.dctId != null) || (this.dctId != null && !this.dctId.equals(other.dctId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ec.Entidades.DetalleCatalogoTipo[ dctId=" + dctId + " ]";
    }
    
}

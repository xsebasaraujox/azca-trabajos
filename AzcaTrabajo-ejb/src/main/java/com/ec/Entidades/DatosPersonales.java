/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sebastian
 */
@Entity
@Table(name = "datos_personales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DatosPersonales.findAll", query = "SELECT d FROM DatosPersonales d"),
    @NamedQuery(name = "DatosPersonales.findByDpId", query = "SELECT d FROM DatosPersonales d WHERE d.dpId = :dpId"),
    @NamedQuery(name = "DatosPersonales.findByDpNombre", query = "SELECT d FROM DatosPersonales d WHERE d.dpNombre = :dpNombre"),
    @NamedQuery(name = "DatosPersonales.findByDpEdad", query = "SELECT d FROM DatosPersonales d WHERE d.dpEdad = :dpEdad"),
    @NamedQuery(name = "DatosPersonales.findByDpEstadocivil", query = "SELECT d FROM DatosPersonales d WHERE d.dpEstadocivil = :dpEstadocivil"),
    @NamedQuery(name = "DatosPersonales.findByDpCedula", query = "SELECT d FROM DatosPersonales d WHERE d.dpCedula = :dpCedula"),
    @NamedQuery(name = "DatosPersonales.findByDpTelefono", query = "SELECT d FROM DatosPersonales d WHERE d.dpTelefono = :dpTelefono"),
    @NamedQuery(name = "DatosPersonales.findByDpCelular", query = "SELECT d FROM DatosPersonales d WHERE d.dpCelular = :dpCelular"),
    @NamedQuery(name = "DatosPersonales.findByDpCorreo", query = "SELECT d FROM DatosPersonales d WHERE d.dpCorreo = :dpCorreo"),
    @NamedQuery(name = "DatosPersonales.findByDpLatitud", query = "SELECT d FROM DatosPersonales d WHERE d.dpLatitud = :dpLatitud"),
    @NamedQuery(name = "DatosPersonales.findByDpLonguitud", query = "SELECT d FROM DatosPersonales d WHERE d.dpLonguitud = :dpLonguitud"),
    @NamedQuery(name = "DatosPersonales.findByDpPresentacion", query = "SELECT d FROM DatosPersonales d WHERE d.dpPresentacion = :dpPresentacion"),
    @NamedQuery(name = "DatosPersonales.findByDpPreferenciasalarial", query = "SELECT d FROM DatosPersonales d WHERE d.dpPreferenciasalarial = :dpPreferenciasalarial"),
    @NamedQuery(name = "DatosPersonales.findByDpFoto", query = "SELECT d FROM DatosPersonales d WHERE d.dpFoto = :dpFoto")})
public class DatosPersonales implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DP_ID")
    private Integer dpId;
    @Size(max = 50)
    @Column(name = "DP_NOMBRE")
    private String dpNombre;
    @Column(name = "DP_EDAD")
    private Integer dpEdad;
    @Size(max = 50)
    @Column(name = "DP_ESTADOCIVIL")
    private String dpEstadocivil;
    @Size(max = 10)
    @Column(name = "DP_CEDULA")
    private String dpCedula;
    @Size(max = 10)
    @Column(name = "DP_TELEFONO")
    private String dpTelefono;
    @Size(max = 10)
    @Column(name = "DP_CELULAR")
    private String dpCelular;
    @Size(max = 50)
    @Column(name = "DP_CORREO")
    private String dpCorreo;
    @Column(name = "DP_LATITUD")
    private Integer dpLatitud;
    @Column(name = "DP_LONGUITUD")
    private Integer dpLonguitud;
    @Size(max = 500)
    @Column(name = "DP_PRESENTACION")
    private String dpPresentacion;
    @Column(name = "DP_PREFERENCIASALARIAL")
    private Integer dpPreferenciasalarial;
    @Size(max = 50)
    @Column(name = "DP_FOTO")
    private String dpFoto;
    @OneToMany(mappedBy = "dpId")
    private List<Postulante> postulanteList;

    public DatosPersonales() {
    }

    public DatosPersonales(Integer dpId) {
        this.dpId = dpId;
    }

    public Integer getDpId() {
        return dpId;
    }

    public void setDpId(Integer dpId) {
        this.dpId = dpId;
    }

    public String getDpNombre() {
        return dpNombre;
    }

    public void setDpNombre(String dpNombre) {
        this.dpNombre = dpNombre;
    }

    public Integer getDpEdad() {
        return dpEdad;
    }

    public void setDpEdad(Integer dpEdad) {
        this.dpEdad = dpEdad;
    }

    public String getDpEstadocivil() {
        return dpEstadocivil;
    }

    public void setDpEstadocivil(String dpEstadocivil) {
        this.dpEstadocivil = dpEstadocivil;
    }

    public String getDpCedula() {
        return dpCedula;
    }

    public void setDpCedula(String dpCedula) {
        this.dpCedula = dpCedula;
    }

    public String getDpTelefono() {
        return dpTelefono;
    }

    public void setDpTelefono(String dpTelefono) {
        this.dpTelefono = dpTelefono;
    }

    public String getDpCelular() {
        return dpCelular;
    }

    public void setDpCelular(String dpCelular) {
        this.dpCelular = dpCelular;
    }

    public String getDpCorreo() {
        return dpCorreo;
    }

    public void setDpCorreo(String dpCorreo) {
        this.dpCorreo = dpCorreo;
    }

    public Integer getDpLatitud() {
        return dpLatitud;
    }

    public void setDpLatitud(Integer dpLatitud) {
        this.dpLatitud = dpLatitud;
    }

    public Integer getDpLonguitud() {
        return dpLonguitud;
    }

    public void setDpLonguitud(Integer dpLonguitud) {
        this.dpLonguitud = dpLonguitud;
    }

    public String getDpPresentacion() {
        return dpPresentacion;
    }

    public void setDpPresentacion(String dpPresentacion) {
        this.dpPresentacion = dpPresentacion;
    }

    public Integer getDpPreferenciasalarial() {
        return dpPreferenciasalarial;
    }

    public void setDpPreferenciasalarial(Integer dpPreferenciasalarial) {
        this.dpPreferenciasalarial = dpPreferenciasalarial;
    }

    public String getDpFoto() {
        return dpFoto;
    }

    public void setDpFoto(String dpFoto) {
        this.dpFoto = dpFoto;
    }

    @XmlTransient
    public List<Postulante> getPostulanteList() {
        return postulanteList;
    }

    public void setPostulanteList(List<Postulante> postulanteList) {
        this.postulanteList = postulanteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dpId != null ? dpId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatosPersonales)) {
            return false;
        }
        DatosPersonales other = (DatosPersonales) object;
        if ((this.dpId == null && other.dpId != null) || (this.dpId != null && !this.dpId.equals(other.dpId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ec.Entidades.DatosPersonales[ dpId=" + dpId + " ]";
    }
    
}

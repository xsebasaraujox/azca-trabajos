/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sebastian
 */
@Entity
@Table(name = "postulante")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Postulante.findAll", query = "SELECT p FROM Postulante p"),
    @NamedQuery(name = "Postulante.findByPosId", query = "SELECT p FROM Postulante p WHERE p.posId = :posId")})
public class Postulante implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "POS_ID")
    private Integer posId;
    @OneToMany(mappedBy = "posId")
    private List<Informatica> informaticaList;
    @OneToMany(mappedBy = "posId")
    private List<Referencias> referenciasList;
    @OneToMany(mappedBy = "posId")
    private List<UserRole> userRoleList;
    @OneToMany(mappedBy = "posId")
    private List<Idiomas> idiomasList;
    @OneToMany(mappedBy = "posId")
    private List<Registro> registroList;
    @OneToMany(mappedBy = "posId")
    private List<ExperienciaLaboral> experienciaLaboralList;
    @JoinColumn(name = "DP_ID", referencedColumnName = "DP_ID")
    @ManyToOne
    private DatosPersonales dpId;
    @OneToMany(mappedBy = "posId")
    private List<ConocimientoAdicional> conocimientoAdicionalList;
    @OneToMany(mappedBy = "posId")
    private List<Estudios> estudiosList;

    public Postulante() {
    }

    public Postulante(Integer posId) {
        this.posId = posId;
    }

    public Integer getPosId() {
        return posId;
    }

    public void setPosId(Integer posId) {
        this.posId = posId;
    }

    @XmlTransient
    public List<Informatica> getInformaticaList() {
        return informaticaList;
    }

    public void setInformaticaList(List<Informatica> informaticaList) {
        this.informaticaList = informaticaList;
    }

    @XmlTransient
    public List<Referencias> getReferenciasList() {
        return referenciasList;
    }

    public void setReferenciasList(List<Referencias> referenciasList) {
        this.referenciasList = referenciasList;
    }

    @XmlTransient
    public List<UserRole> getUserRoleList() {
        return userRoleList;
    }

    public void setUserRoleList(List<UserRole> userRoleList) {
        this.userRoleList = userRoleList;
    }

    @XmlTransient
    public List<Idiomas> getIdiomasList() {
        return idiomasList;
    }

    public void setIdiomasList(List<Idiomas> idiomasList) {
        this.idiomasList = idiomasList;
    }

    @XmlTransient
    public List<Registro> getRegistroList() {
        return registroList;
    }

    public void setRegistroList(List<Registro> registroList) {
        this.registroList = registroList;
    }

    @XmlTransient
    public List<ExperienciaLaboral> getExperienciaLaboralList() {
        return experienciaLaboralList;
    }

    public void setExperienciaLaboralList(List<ExperienciaLaboral> experienciaLaboralList) {
        this.experienciaLaboralList = experienciaLaboralList;
    }

    public DatosPersonales getDpId() {
        return dpId;
    }

    public void setDpId(DatosPersonales dpId) {
        this.dpId = dpId;
    }

    @XmlTransient
    public List<ConocimientoAdicional> getConocimientoAdicionalList() {
        return conocimientoAdicionalList;
    }

    public void setConocimientoAdicionalList(List<ConocimientoAdicional> conocimientoAdicionalList) {
        this.conocimientoAdicionalList = conocimientoAdicionalList;
    }

    @XmlTransient
    public List<Estudios> getEstudiosList() {
        return estudiosList;
    }

    public void setEstudiosList(List<Estudios> estudiosList) {
        this.estudiosList = estudiosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (posId != null ? posId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Postulante)) {
            return false;
        }
        Postulante other = (Postulante) object;
        if ((this.posId == null && other.posId != null) || (this.posId != null && !this.posId.equals(other.posId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ec.Entidades.Postulante[ posId=" + posId + " ]";
    }
    
}

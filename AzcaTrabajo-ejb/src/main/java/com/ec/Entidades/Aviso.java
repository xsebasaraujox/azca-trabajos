/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Entidades;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sebastian
 */
@Entity
@Table(name = "aviso")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Aviso.findAll", query = "SELECT a FROM Aviso a"),
    @NamedQuery(name = "Aviso.findByAvId", query = "SELECT a FROM Aviso a WHERE a.avId = :avId"),
    @NamedQuery(name = "Aviso.findByAvFecha", query = "SELECT a FROM Aviso a WHERE a.avFecha = :avFecha"),
    @NamedQuery(name = "Aviso.findByAvSalario", query = "SELECT a FROM Aviso a WHERE a.avSalario = :avSalario"),
    @NamedQuery(name = "Aviso.findByAvDescripcion", query = "SELECT a FROM Aviso a WHERE a.avDescripcion = :avDescripcion")})
public class Aviso implements Serializable {
    @Size(max = 100)
    @Column(name = "AV_TITULO")
    private String avTitulo;
    @Column(name = "AV_DISCAPACITADO")
    private Boolean avDiscapacitado;
    @Size(max = 50)
    @Column(name = "AV_TIPO_PUESTO")
    private String avTipoPuesto;
    @OneToMany(mappedBy = "avId")
    private Collection<Preguntas> preguntasCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "AV_ID")
    private Integer avId;
    @Column(name = "AV_FECHA")
    @Temporal(TemporalType.DATE)
    private Date avFecha;
    @Column(name = "AV_SALARIO")
    private String avSalario;
    @Size(max = 800)
    @Column(name = "AV_DESCRIPCION")
    private String avDescripcion;
    @OneToMany(mappedBy = "avId")
    private List<Registro> registroList;
    @JoinColumn(name = "SA_ID", referencedColumnName = "SA_ID")
    @ManyToOne
    private Subarea saId;
    @JoinColumn(name = "PRO_ID", referencedColumnName = "PRO_ID")
    @ManyToOne
    private Provincia proId;
    @JoinColumn(name = "EMP_ID", referencedColumnName = "EMP_ID")
    @ManyToOne
    private Empresa empId;
    @JoinColumn(name = "AR_ID", referencedColumnName = "AR_ID")
    @ManyToOne
    private Area arId;
    @JoinColumn(name = "DC_ID", referencedColumnName = "DC_ID")
    @ManyToOne
    private DetalleCatalogo dcId;

    public Aviso() {
    }

    public Aviso(Integer avId) {
        this.avId = avId;
    }

    public Integer getAvId() {
        return avId;
    }

    public void setAvId(Integer avId) {
        this.avId = avId;
    }

    public Date getAvFecha() {
        return avFecha;
    }

    public void setAvFecha(Date avFecha) {
        this.avFecha = avFecha;
    }

    public String getAvSalario() {
        return avSalario;
    }

    public void setAvSalario(String avSalario) {
        this.avSalario = avSalario;
    }

    public String getAvDescripcion() {
        return avDescripcion;
    }

    public void setAvDescripcion(String avDescripcion) {
        this.avDescripcion = avDescripcion;
    }

    @XmlTransient
    public List<Registro> getRegistroList() {
        return registroList;
    }

    public void setRegistroList(List<Registro> registroList) {
        this.registroList = registroList;
    }

    public Subarea getSaId() {
        return saId;
    }

    public void setSaId(Subarea saId) {
        this.saId = saId;
    }

    public Provincia getProId() {
        return proId;
    }

    public void setProId(Provincia proId) {
        this.proId = proId;
    }

    public Empresa getEmpId() {
        return empId;
    }

    public void setEmpId(Empresa empId) {
        this.empId = empId;
    }

    public Area getArId() {
        return arId;
    }

    public void setArId(Area arId) {
        this.arId = arId;
    }

    public DetalleCatalogo getDcId() {
        return dcId;
    }

    public void setDcId(DetalleCatalogo dcId) {
        this.dcId = dcId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (avId != null ? avId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aviso)) {
            return false;
        }
        Aviso other = (Aviso) object;
        if ((this.avId == null && other.avId != null) || (this.avId != null && !this.avId.equals(other.avId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ec.Entidades.Aviso[ avId=" + avId + " ]";
    }

    public String getAvTitulo() {
        return avTitulo;
    }

    public void setAvTitulo(String avTitulo) {
        this.avTitulo = avTitulo;
    }

    public Boolean getAvDiscapacitado() {
        return avDiscapacitado;
    }

    public void setAvDiscapacitado(Boolean avDiscapacitado) {
        this.avDiscapacitado = avDiscapacitado;
    }

    public String getAvTipoPuesto() {
        return avTipoPuesto;
    }

    public void setAvTipoPuesto(String avTipoPuesto) {
        this.avTipoPuesto = avTipoPuesto;
    }

    @XmlTransient
    public Collection<Preguntas> getPreguntasCollection() {
        return preguntasCollection;
    }

    public void setPreguntasCollection(Collection<Preguntas> preguntasCollection) {
        this.preguntasCollection = preguntasCollection;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author sebastian
 */
@Entity
@Table(name = "detalle_catalogo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalleCatalogo.findAll", query = "SELECT d FROM DetalleCatalogo d"),
    @NamedQuery(name = "DetalleCatalogo.findByDcId", query = "SELECT d FROM DetalleCatalogo d WHERE d.dcId = :dcId"),
    @NamedQuery(name = "DetalleCatalogo.findByDcDescripcion", query = "SELECT d FROM DetalleCatalogo d WHERE d.dcDescripcion = :dcDescripcion")})
public class DetalleCatalogo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "DC_ID")
    private Integer dcId;
    @Size(max = 50)
    @Column(name = "DC_DESCRIPCION")
    private String dcDescripcion;
    @OneToMany(mappedBy = "nivel")
    private List<Informatica> informaticaList;
    @OneToMany(mappedBy = "tipo")
    private List<Referencias> referenciasList;
    @OneToMany(mappedBy = "relacion")
    private List<Referencias> referenciasList1;
    @JoinColumn(name = "DCT_ID", referencedColumnName = "DCT_ID")
    @ManyToOne
    private DetalleCatalogoTipo dctId;
    @OneToMany(mappedBy = "idioma")
    private List<Idiomas> idiomasList;
    @OneToMany(mappedBy = "escritoNivel")
    private List<Idiomas> idiomasList1;
    @OneToMany(mappedBy = "oralNivel")
    private List<Idiomas> idiomasList2;
    @OneToMany(mappedBy = "senority")
    private List<ExperienciaLaboral> experienciaLaboralList;
    @OneToMany(mappedBy = "industria")
    private List<ExperienciaLaboral> experienciaLaboralList1;
    @OneToMany(mappedBy = "nivel")
    private List<Estudios> estudiosList;
    @OneToMany(mappedBy = "estado")
    private List<Estudios> estudiosList1;
    @OneToMany(mappedBy = "institucion")
    private List<Estudios> estudiosList2;
    @OneToMany(mappedBy = "dcId")
    private List<Aviso> avisoList;

    public DetalleCatalogo() {
    }

    public DetalleCatalogo(Integer dcId) {
        this.dcId = dcId;
    }

    public Integer getDcId() {
        return dcId;
    }

    public void setDcId(Integer dcId) {
        this.dcId = dcId;
    }

    public String getDcDescripcion() {
        return dcDescripcion;
    }

    public void setDcDescripcion(String dcDescripcion) {
        this.dcDescripcion = dcDescripcion;
    }

    @XmlTransient
    public List<Informatica> getInformaticaList() {
        return informaticaList;
    }

    public void setInformaticaList(List<Informatica> informaticaList) {
        this.informaticaList = informaticaList;
    }

    @XmlTransient
    public List<Referencias> getReferenciasList() {
        return referenciasList;
    }

    public void setReferenciasList(List<Referencias> referenciasList) {
        this.referenciasList = referenciasList;
    }

    @XmlTransient
    public List<Referencias> getReferenciasList1() {
        return referenciasList1;
    }

    public void setReferenciasList1(List<Referencias> referenciasList1) {
        this.referenciasList1 = referenciasList1;
    }

    public DetalleCatalogoTipo getDctId() {
        return dctId;
    }

    public void setDctId(DetalleCatalogoTipo dctId) {
        this.dctId = dctId;
    }

    @XmlTransient
    public List<Idiomas> getIdiomasList() {
        return idiomasList;
    }

    public void setIdiomasList(List<Idiomas> idiomasList) {
        this.idiomasList = idiomasList;
    }

    @XmlTransient
    public List<Idiomas> getIdiomasList1() {
        return idiomasList1;
    }

    public void setIdiomasList1(List<Idiomas> idiomasList1) {
        this.idiomasList1 = idiomasList1;
    }

    @XmlTransient
    public List<Idiomas> getIdiomasList2() {
        return idiomasList2;
    }

    public void setIdiomasList2(List<Idiomas> idiomasList2) {
        this.idiomasList2 = idiomasList2;
    }

    @XmlTransient
    public List<ExperienciaLaboral> getExperienciaLaboralList() {
        return experienciaLaboralList;
    }

    public void setExperienciaLaboralList(List<ExperienciaLaboral> experienciaLaboralList) {
        this.experienciaLaboralList = experienciaLaboralList;
    }

    @XmlTransient
    public List<ExperienciaLaboral> getExperienciaLaboralList1() {
        return experienciaLaboralList1;
    }

    public void setExperienciaLaboralList1(List<ExperienciaLaboral> experienciaLaboralList1) {
        this.experienciaLaboralList1 = experienciaLaboralList1;
    }

    @XmlTransient
    public List<Estudios> getEstudiosList() {
        return estudiosList;
    }

    public void setEstudiosList(List<Estudios> estudiosList) {
        this.estudiosList = estudiosList;
    }

    @XmlTransient
    public List<Estudios> getEstudiosList1() {
        return estudiosList1;
    }

    public void setEstudiosList1(List<Estudios> estudiosList1) {
        this.estudiosList1 = estudiosList1;
    }

    @XmlTransient
    public List<Estudios> getEstudiosList2() {
        return estudiosList2;
    }

    public void setEstudiosList2(List<Estudios> estudiosList2) {
        this.estudiosList2 = estudiosList2;
    }

    @XmlTransient
    public List<Aviso> getAvisoList() {
        return avisoList;
    }

    public void setAvisoList(List<Aviso> avisoList) {
        this.avisoList = avisoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dcId != null ? dcId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleCatalogo)) {
            return false;
        }
        DetalleCatalogo other = (DetalleCatalogo) object;
        if ((this.dcId == null && other.dcId != null) || (this.dcId != null && !this.dcId.equals(other.dcId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ec.Entidades.DetalleCatalogo[ dcId=" + dcId + " ]";
    }
    
}

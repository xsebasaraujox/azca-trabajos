/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sebastian
 */
@Entity
@Table(name = "informatica")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Informatica.findAll", query = "SELECT i FROM Informatica i"),
    @NamedQuery(name = "Informatica.findByInId", query = "SELECT i FROM Informatica i WHERE i.inId = :inId")})
public class Informatica implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IN_ID")
    private Integer inId;
    @JoinColumn(name = "POS_ID", referencedColumnName = "POS_ID")
    @ManyToOne
    private Postulante posId;
    @JoinColumn(name = "NIVEL", referencedColumnName = "DC_ID")
    @ManyToOne
    private DetalleCatalogo nivel;
    @JoinColumn(name = "AR_ID", referencedColumnName = "AR_ID")
    @ManyToOne
    private Area arId;
    @JoinColumn(name = "SA_ID", referencedColumnName = "SA_ID")
    @ManyToOne
    private Subarea saId;

    public Informatica() {
    }

    public Informatica(Integer inId) {
        this.inId = inId;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public Postulante getPosId() {
        return posId;
    }

    public void setPosId(Postulante posId) {
        this.posId = posId;
    }

    public DetalleCatalogo getNivel() {
        return nivel;
    }

    public void setNivel(DetalleCatalogo nivel) {
        this.nivel = nivel;
    }

    public Area getArId() {
        return arId;
    }

    public void setArId(Area arId) {
        this.arId = arId;
    }

    public Subarea getSaId() {
        return saId;
    }

    public void setSaId(Subarea saId) {
        this.saId = saId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inId != null ? inId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Informatica)) {
            return false;
        }
        Informatica other = (Informatica) object;
        if ((this.inId == null && other.inId != null) || (this.inId != null && !this.inId.equals(other.inId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ec.Entidades.Informatica[ inId=" + inId + " ]";
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sebastian
 */
@Entity
@Table(name = "conocimiento_adicional")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConocimientoAdicional.findAll", query = "SELECT c FROM ConocimientoAdicional c"),
    @NamedQuery(name = "ConocimientoAdicional.findByCaId", query = "SELECT c FROM ConocimientoAdicional c WHERE c.caId = :caId"),
    @NamedQuery(name = "ConocimientoAdicional.findByCaNombre", query = "SELECT c FROM ConocimientoAdicional c WHERE c.caNombre = :caNombre"),
    @NamedQuery(name = "ConocimientoAdicional.findByCaDescripcion", query = "SELECT c FROM ConocimientoAdicional c WHERE c.caDescripcion = :caDescripcion")})
public class ConocimientoAdicional implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CA_ID")
    private Integer caId;
    @Size(max = 50)
    @Column(name = "CA_NOMBRE")
    private String caNombre;
    @Size(max = 200)
    @Column(name = "CA_DESCRIPCION")
    private String caDescripcion;
    @JoinColumn(name = "POS_ID", referencedColumnName = "POS_ID")
    @ManyToOne
    private Postulante posId;

    public ConocimientoAdicional() {
    }

    public ConocimientoAdicional(Integer caId) {
        this.caId = caId;
    }

    public Integer getCaId() {
        return caId;
    }

    public void setCaId(Integer caId) {
        this.caId = caId;
    }

    public String getCaNombre() {
        return caNombre;
    }

    public void setCaNombre(String caNombre) {
        this.caNombre = caNombre;
    }

    public String getCaDescripcion() {
        return caDescripcion;
    }

    public void setCaDescripcion(String caDescripcion) {
        this.caDescripcion = caDescripcion;
    }

    public Postulante getPosId() {
        return posId;
    }

    public void setPosId(Postulante posId) {
        this.posId = posId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (caId != null ? caId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConocimientoAdicional)) {
            return false;
        }
        ConocimientoAdicional other = (ConocimientoAdicional) object;
        if ((this.caId == null && other.caId != null) || (this.caId != null && !this.caId.equals(other.caId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ec.Entidades.ConocimientoAdicional[ caId=" + caId + " ]";
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosLocal;

import com.ec.Entidades.Area;
import com.ec.Entidades.Subarea;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Xavier
 */
@Local
public interface SubareaFacadeLocal {

    void create(Subarea subarea);

    void edit(Subarea subarea);

    void remove(Subarea subarea);

    Subarea find(Object id);

    List<Subarea> findAll();

    List<Subarea> findRange(int[] range);

    int count();
    
    List<Subarea> obtenerSubareasPorArea(Area area);
    
    public Subarea obtenerSubareasPorId(Integer id);
}

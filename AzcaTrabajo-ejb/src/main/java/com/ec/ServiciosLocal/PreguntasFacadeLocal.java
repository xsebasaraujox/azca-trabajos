/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosLocal;

import com.ec.Entidades.Preguntas;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Xavier
 */
@Local
public interface PreguntasFacadeLocal {

    void create(Preguntas preguntas);

    void edit(Preguntas preguntas);

    void remove(Preguntas preguntas);

    Preguntas find(Object id);

    List<Preguntas> findAll();

    List<Preguntas> findRange(int[] range);

    int count();
    
}

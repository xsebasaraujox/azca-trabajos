/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosLocal;

import com.ec.Entidades.Informatica;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Xavier
 */
@Local
public interface InformaticaFacadeLocal {

    void create(Informatica informatica);

    void edit(Informatica informatica);

    void remove(Informatica informatica);

    Informatica find(Object id);

    List<Informatica> findAll();

    List<Informatica> findRange(int[] range);

    int count();
    
}

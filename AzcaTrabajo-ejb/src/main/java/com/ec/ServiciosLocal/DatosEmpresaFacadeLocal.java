/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosLocal;

import com.ec.Entidades.DatosEmpresa;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Xavier
 */
@Local
public interface DatosEmpresaFacadeLocal {

    void create(DatosEmpresa datosEmpresa);

    void edit(DatosEmpresa datosEmpresa);

    void remove(DatosEmpresa datosEmpresa);

    DatosEmpresa find(Object id);

    List<DatosEmpresa> findAll();

    List<DatosEmpresa> findRange(int[] range);

    int count();
    
}

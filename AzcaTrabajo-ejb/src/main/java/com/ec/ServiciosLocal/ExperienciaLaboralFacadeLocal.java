/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosLocal;

import com.ec.Entidades.ExperienciaLaboral;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Xavier
 */
@Local
public interface ExperienciaLaboralFacadeLocal {

    void create(ExperienciaLaboral experienciaLaboral);

    void edit(ExperienciaLaboral experienciaLaboral);

    void remove(ExperienciaLaboral experienciaLaboral);

    ExperienciaLaboral find(Object id);

    List<ExperienciaLaboral> findAll();

    List<ExperienciaLaboral> findRange(int[] range);

    int count();
    
}

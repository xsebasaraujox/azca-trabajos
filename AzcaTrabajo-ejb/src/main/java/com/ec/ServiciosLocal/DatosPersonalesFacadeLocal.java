/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosLocal;

import com.ec.Entidades.DatosPersonales;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Xavier
 */
@Local
public interface DatosPersonalesFacadeLocal {

    void create(DatosPersonales datosPersonales);

    void edit(DatosPersonales datosPersonales);

    void remove(DatosPersonales datosPersonales);

    DatosPersonales find(Object id);

    List<DatosPersonales> findAll();

    List<DatosPersonales> findRange(int[] range);

    int count();
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosLocal;

import com.ec.Entidades.ConocimientoAdicional;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Xavier
 */
@Local
public interface ConocimientoAdicionalFacadeLocal {

    void create(ConocimientoAdicional conocimientoAdicional);

    void edit(ConocimientoAdicional conocimientoAdicional);

    void remove(ConocimientoAdicional conocimientoAdicional);

    ConocimientoAdicional find(Object id);

    List<ConocimientoAdicional> findAll();

    List<ConocimientoAdicional> findRange(int[] range);

    int count();
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosLocal;

import com.ec.Entidades.Pais;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Xavier
 */
@Local
public interface PaisFacadeLocal {

    void create(Pais pais);

    void edit(Pais pais);

    void remove(Pais pais);

    Pais find(Object id);

    List<Pais> findAll();

    List<Pais> findRange(int[] range);

    int count();
    
}

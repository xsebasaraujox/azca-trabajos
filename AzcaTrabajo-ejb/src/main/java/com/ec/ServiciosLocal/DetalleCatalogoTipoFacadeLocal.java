/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosLocal;

import com.ec.Entidades.DetalleCatalogoTipo;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Xavier
 */
@Local
public interface DetalleCatalogoTipoFacadeLocal {

    void create(DetalleCatalogoTipo detalleCatalogoTipo);

    void edit(DetalleCatalogoTipo detalleCatalogoTipo);

    void remove(DetalleCatalogoTipo detalleCatalogoTipo);

    DetalleCatalogoTipo find(Object id);

    List<DetalleCatalogoTipo> findAll();

    List<DetalleCatalogoTipo> findRange(int[] range);

    int count();
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosLocal;

import com.ec.Entidades.Estudios;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Xavier
 */
@Local
public interface EstudiosFacadeLocal {

    void create(Estudios estudios);

    void edit(Estudios estudios);

    void remove(Estudios estudios);

    Estudios find(Object id);

    List<Estudios> findAll();

    List<Estudios> findRange(int[] range);

    int count();
    
}

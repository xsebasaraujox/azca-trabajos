/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosLocal;

import com.ec.Entidades.Idiomas;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Xavier
 */
@Local
public interface IdiomasFacadeLocal {

    void create(Idiomas idiomas);

    void edit(Idiomas idiomas);

    void remove(Idiomas idiomas);

    Idiomas find(Object id);

    List<Idiomas> findAll();

    List<Idiomas> findRange(int[] range);

    int count();
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosLocal;

import com.ec.Entidades.Postulante;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Xavier
 */
@Local
public interface PostulanteFacadeLocal {

    void create(Postulante postulante);

    void edit(Postulante postulante);

    void remove(Postulante postulante);

    Postulante find(Object id);

    List<Postulante> findAll();

    List<Postulante> findRange(int[] range);

    int count();
    
}

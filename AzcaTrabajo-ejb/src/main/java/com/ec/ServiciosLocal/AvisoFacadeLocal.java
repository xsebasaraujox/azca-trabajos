/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosLocal;

import com.ec.Entidades.Aviso;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Xavier
 */
@Local
public interface AvisoFacadeLocal {

    void create(Aviso aviso);

    void edit(Aviso aviso);

    void remove(Aviso aviso);

    Aviso find(Object id);

    List<Aviso> findAll();

    List<Aviso> findRange(int[] range);

    int count();
    
}

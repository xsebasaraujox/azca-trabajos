/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosLocal;

import com.ec.Entidades.DetalleCatalogo;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Xavier
 */
@Local
public interface DetalleCatalogoFacadeLocal {

    void create(DetalleCatalogo detalleCatalogo);

    void edit(DetalleCatalogo detalleCatalogo);

    void remove(DetalleCatalogo detalleCatalogo);

    DetalleCatalogo find(Object id);

    List<DetalleCatalogo> findAll();

    List<DetalleCatalogo> findRange(int[] range);

    int count();
    
}

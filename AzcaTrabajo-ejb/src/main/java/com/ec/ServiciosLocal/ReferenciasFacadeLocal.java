/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.ServiciosLocal;

import com.ec.Entidades.Referencias;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Xavier
 */
@Local
public interface ReferenciasFacadeLocal {

    void create(Referencias referencias);

    void edit(Referencias referencias);

    void remove(Referencias referencias);

    Referencias find(Object id);

    List<Referencias> findAll();

    List<Referencias> findRange(int[] range);

    int count();
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.DataManagers;

import com.ec.Entidades.Aviso;
import java.io.Serializable;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Xavier
 */
@ManagedBean
@SessionScoped
public class NuevoAvisoDataManager implements Serializable {

    public static final String KEY = "nuevoAvisoDataManager";
    private Logger LOG = Logger.getLogger(NuevoAvisoDataManager.class.getName());
    private Aviso aviso;
    private String salarioMin;
    private String salarioMax;
    private Boolean salario;
    private Boolean preguntas;
    private String pregunta;
    private String pregunta2;
    private String pregunta3;
    private Boolean preguntas2;
    private Boolean preguntas1;
    private Boolean preguntas3;
    private int contadorPreguntas;

    /**
     * Creates a new instance of nuevoAvisoDataManager
     */
    public NuevoAvisoDataManager() {
        aviso = new Aviso();
        aviso.setAvTipoPuesto("Full-time");
        salarioMax = "";
        salarioMin = "";
        salario = false;
        preguntas = false;
        preguntas1 = false;
        preguntas2 = false;
        preguntas3 = false;
        pregunta = "";
        pregunta2 = "";
        pregunta3 = "";
        contadorPreguntas = 0;
    }

    public Aviso getAviso() {
        return aviso;
    }

    public void setAviso(Aviso aviso) {
        this.aviso = aviso;
    }

    public String getSalarioMin() {
        return salarioMin;
    }

    public void setSalarioMin(String salarioMin) {
        this.salarioMin = salarioMin;
    }

    public String getSalarioMax() {
        return salarioMax;
    }

    public void setSalarioMax(String salarioMax) {
        this.salarioMax = salarioMax;
    }

    public Boolean getSalario() {
        return salario;
    }

    public void setSalario(Boolean salario) {
        this.salario = salario;
    }

    public Boolean getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(Boolean preguntas) {
        this.preguntas = preguntas;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public Boolean getPreguntas2() {
        return preguntas2;
    }

    public void setPreguntas2(Boolean preguntas2) {
        this.preguntas2 = preguntas2;
    }

    public Boolean getPreguntas3() {
        return preguntas3;
    }

    public void setPreguntas3(Boolean preguntas3) {
        this.preguntas3 = preguntas3;
    }

    public int getContadorPreguntas() {
        return contadorPreguntas;
    }

    public void setContadorPreguntas(int contadorPreguntas) {
        this.contadorPreguntas = contadorPreguntas;
    }

    public String getPregunta2() {
        return pregunta2;
    }

    public void setPregunta2(String pregunta2) {
        this.pregunta2 = pregunta2;
    }

    public String getPregunta3() {
        return pregunta3;
    }

    public void setPregunta3(String pregunta3) {
        this.pregunta3 = pregunta3;
    }

    public Boolean getPreguntas1() {
        return preguntas1;
    }

    public void setPreguntas1(Boolean preguntas1) {
        this.preguntas1 = preguntas1;
    }
}

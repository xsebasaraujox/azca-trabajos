/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Validadores;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.apache.log4j.Logger;

/**
 *
 * @author Xavier
 */
@ManagedBean
@RequestScoped
public class NumeroPositivoValidador implements Validator {
    private static final Logger LOG = Logger.getLogger(NumeroValidador.class.getName());
    

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object obj) throws ValidatorException {

        try {
            String numero = obj.toString();
            Double.parseDouble(numero);
            if(Double.parseDouble(numero) < 0)
            {
                FacesMessage msg =
                    new FacesMessage("Debe ingresar un dato númerico positivo",
                    "");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
            }


        } catch (NumberFormatException e) {
             FacesMessage msg =
                    new FacesMessage("Debe ingresar un dato númerico positivo",
                    "");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(msg);
        }

    }
}



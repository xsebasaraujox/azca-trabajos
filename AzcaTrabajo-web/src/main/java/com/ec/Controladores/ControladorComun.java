/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Controladores;

import com.ec.DataManagers.NuevoAvisoDataManager;
import com.ec.Interfaces.Imprimible;
import com.ec.ServiciosLocal.AreaFacadeLocal;
import com.ec.ServiciosLocal.SubareaFacadeLocal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

/**
 *
 * @author Xavier
 */
public abstract class ControladorComun {

    private Logger LOG = Logger.getLogger(ControladorComun.class.getName());
    @ManagedProperty(value = "#{ingresoClienteDataManager}")
    private NuevoAvisoDataManager nuevoAvisoDataManager;
    @EJB
    private AreaFacadeLocal areaFacadeLocal;
    @EJB
    private SubareaFacadeLocal subareaFacadeLocal;

    /**
     * Creates a new instance of ControladorComun
     */
    public ControladorComun() {
    }

    public abstract void iniciarDatos();

    protected abstract void imprimirLogInfo(String mensaje);

    protected abstract void imprimirLogError(String mensaje);

    protected void infoMessage(String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, ""));
    }

    protected void errorMessage(String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, ""));
    }   
    
    protected List<SelectItem> generarItemsConValorNulo(List<? extends Imprimible> lista, String mensaje) {
        List<SelectItem> items = new ArrayList<SelectItem>();
        items.add(new SelectItem(null, mensaje));
        items.addAll(generarItems(lista));
        return items;
    }

    protected List<SelectItem> generarItems(List<? extends Imprimible> lista) {
        List<SelectItem> items = new ArrayList<SelectItem>();
        for (Imprimible imprimible : lista) {
            items.add(new SelectItem(imprimible.getObjecto(), imprimible.getEtiqueta()));
        }
        return items;
    }

    public NuevoAvisoDataManager getNuevoAvisoDataManager() {
        return nuevoAvisoDataManager;
    }

    public void setNuevoAvisoDataManager(NuevoAvisoDataManager nuevoAvisoDataManager) {
        this.nuevoAvisoDataManager = nuevoAvisoDataManager;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Controladores;

import com.ec.DataManagers.NuevoAvisoDataManager;
import com.ec.Entidades.Aviso;
import com.ec.ServiciosLocal.AreaFacadeLocal;
import com.ec.ServiciosLocal.ProvinciaFacadeLocal;
import com.ec.ServiciosLocal.SubareaFacadeLocal;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.model.SelectItem;

/**
 *
 * @author Xavier
 */
@ManagedBean
@RequestScoped
public class NuevoAvisoControlador extends ControladorComun {

    private Logger LOG = Logger.getLogger(NuevoAvisoControlador.class.getName());
    @ManagedProperty(value = "#{nuevoAvisoDataManager}")
    private NuevoAvisoDataManager nuevoAvisoDataManager;
    @EJB
    private AreaFacadeLocal areaFacadeLocal;
    @EJB
    private SubareaFacadeLocal subareaFacadeLocal;
    @EJB
    private ProvinciaFacadeLocal provinciaFacadeLocal;

    /**
     * Creates a new instance of CrearAvisoControlador
     */
    public NuevoAvisoControlador() {
    }

    public void tipoPuestoAvisoActualizar(String valor) {
        getNuevoAvisoDataManager().getAviso().setAvTipoPuesto(valor);
    }

    public void agregarPreguntas() {
        getNuevoAvisoDataManager().setContadorPreguntas(getNuevoAvisoDataManager().getContadorPreguntas() + 1);
        if (getNuevoAvisoDataManager().getContadorPreguntas() == 1) {
            getNuevoAvisoDataManager().setPreguntas(true);
        }
        if (getNuevoAvisoDataManager().getContadorPreguntas() == 2) {
            getNuevoAvisoDataManager().setPreguntas2(true);
        }
        if (getNuevoAvisoDataManager().getContadorPreguntas() == 3) {
            getNuevoAvisoDataManager().setPreguntas3(true);
            getNuevoAvisoDataManager().setPreguntas1(true);
        }
    }

    public void eliminarPreguntas() {
        getNuevoAvisoDataManager().setContadorPreguntas(getNuevoAvisoDataManager().getContadorPreguntas() - 1);
        if (getNuevoAvisoDataManager().getContadorPreguntas() == 0) {
            getNuevoAvisoDataManager().setPreguntas(false);
            getNuevoAvisoDataManager().setPreguntas1(false);
        }
        if (getNuevoAvisoDataManager().getContadorPreguntas() == 1) {
            getNuevoAvisoDataManager().setPreguntas2(false);
            getNuevoAvisoDataManager().setPreguntas1(false);
        }
        if (getNuevoAvisoDataManager().getContadorPreguntas() == 2) {
            getNuevoAvisoDataManager().setPreguntas3(false);
            getNuevoAvisoDataManager().setPreguntas1(false);
        }
    }

    public void guardarAviso() {
        try {

            //TODO codigo para guardar OJO

            infoMessage("Aviso creado correctamente");
            limpiarDatos();
        } catch (Exception e) {
            imprimirLogError("..Error al crear aviso");
        }
    }

    public void limpiarDatos() {
        getNuevoAvisoDataManager().setAviso(new Aviso());
        getNuevoAvisoDataManager().getAviso().setAvTipoPuesto("Full-time");
        getNuevoAvisoDataManager().setSalarioMax("");
        getNuevoAvisoDataManager().setSalarioMin("");
        getNuevoAvisoDataManager().setSalario(false);
        getNuevoAvisoDataManager().setPreguntas(false);
        getNuevoAvisoDataManager().setPreguntas1(false);
        getNuevoAvisoDataManager().setPreguntas2(false);
        getNuevoAvisoDataManager().setPreguntas3(false);
        getNuevoAvisoDataManager().setPregunta("");
        getNuevoAvisoDataManager().setPregunta2("");
        getNuevoAvisoDataManager().setPregunta3("");
        getNuevoAvisoDataManager().setContadorPreguntas(0);
    }

    @Override
    protected void imprimirLogInfo(String mensaje) {
        LOG.info(mensaje);
    }

    @Override
    protected void imprimirLogError(String mensaje) {
        LOG.warning("ERROR " + mensaje);
    }

    @Override
    public void iniciarDatos() {
        imprimirLogInfo(" .. iniciarDatos");
    }

    public List<SelectItem> getArea() {
        return generarItemsConValorNulo(areaFacadeLocal.findAll(), "SELECCIONE_AREA");
    }

    public List<SelectItem> getProvincia() {
        return generarItemsConValorNulo(provinciaFacadeLocal.findAll(), "ECUADOR (Todo el país)");
    }

    public List<SelectItem> getSubarea() {
        if (nuevoAvisoDataManager.getAviso().getArId() != null) {
            return generarItemsConValorNulo(subareaFacadeLocal.obtenerSubareasPorArea(nuevoAvisoDataManager.getAviso().getArId()), "SELECCIONE_SUBAREA");
        }
        return null;
    }

    @Override
    public NuevoAvisoDataManager getNuevoAvisoDataManager() {
        return nuevoAvisoDataManager;
    }

    @Override
    public void setNuevoAvisoDataManager(NuevoAvisoDataManager nuevoAvisoDataManager) {
        this.nuevoAvisoDataManager = nuevoAvisoDataManager;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Convertidores;

import com.ec.Entidades.Area;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Xavier
 */
@FacesConverter("areaConvertidor")
public class AreaConvertidor implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null) {
            return null;
        }
        try {
            return new Area(Integer.parseInt(value));
        } catch (NumberFormatException e) {
            return null;
        }

    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return null;
        }
        Area area = (Area) value;        
        return area.getArId().toString();        
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ec.Convertidores;

import com.ec.Entidades.Subarea;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author Xavier
 */
@FacesConverter("subareaConvertidor")
public class SubareaConvertidor implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null) {
            return null;
        }
        try {
            return new Subarea(Integer.parseInt(value));
        } catch (NumberFormatException e) {
            return null;
        }

    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return null;
        }
        Subarea subarea = (Subarea) value;
        return subarea.getSaId().toString();
    }
}

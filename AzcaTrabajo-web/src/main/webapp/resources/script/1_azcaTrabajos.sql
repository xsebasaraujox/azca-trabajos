/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     22/03/2015 14:46:10                          */
/*==============================================================*/
create database AzcaTrabajos;

use AzcaTrabajos;

drop table if exists AREA;

drop table if exists AVISO;

drop table if exists CONOCIMIENTO_ADICIONAL;

drop table if exists DATOS_EMPRESA;

drop table if exists DATOS_PERSONALES;

drop table if exists DETALLE_CATALOGO;

drop table if exists DETALLE_CATALOGO_TIPO;

drop table if exists EMPRESA;

drop table if exists ESTUDIOS;

drop table if exists EXPERIENCIA_LABORAL;

drop table if exists IDIOMAS;

drop table if exists INFORMATICA;

drop table if exists PAIS;

drop table if exists POSTULANTE;

drop table if exists PROVINCIA;

drop table if exists REFERENCIAS;

drop table if exists ROLE;

drop table if exists SUBAREA;

drop table if exists USER;

drop table if exists USER_ROLE;

/*==============================================================*/
/* Table: AREA                                                  */
/*==============================================================*/
create table AREA
(
   AR_ID                numeric(8,0) not null,
   AR_DESCRIPCION       varchar(50),
   primary key (AR_ID)
);

/*==============================================================*/
/* Table: AVISO                                                 */
/*==============================================================*/
create table AVISO
(
   AV_ID                numeric(8,0) not null,
   PRO_ID               numeric(8,0),
   SA_ID                numeric(8,0),
   EMP_ID               numeric(8,0),
   DC_ID                numeric(8,0),
   AR_ID                numeric(8,0),
   AV_FECHA             date,
   AV_SALARIO           numeric(8,0),
   AV_DESCRIPCION       varchar(800),
   primary key (AV_ID)
);

/*==============================================================*/
/* Table: CONOCIMIENTO_ADICIONAL                                */
/*==============================================================*/
create table CONOCIMIENTO_ADICIONAL
(
   CA_ID                numeric(8,0) not null,
   POS_ID               numeric(8,0),
   CA_NOMBRE            varchar(50),
   CA_DESCRIPCION       varchar(200),
   primary key (CA_ID)
);

/*==============================================================*/
/* Table: DATOS_EMPRESA                                         */
/*==============================================================*/
create table DATOS_EMPRESA
(
   DE_ID                numeric(8,0) not null,
   DE_NOMBRE            varchar(50),
   DE_RUC               varchar(13),
   DE_TELEFONO          varchar(10),
   DE_CORREO            varchar(50),
   primary key (DE_ID)
);

/*==============================================================*/
/* Table: DATOS_PERSONALES                                      */
/*==============================================================*/
create table DATOS_PERSONALES
(
   DP_ID                numeric(8,0) not null,
   DP_NOMBRE            varchar(50),
   DP_EDAD              numeric(8,0),
   DP_ESTADOCIVIL       varchar(50),
   DP_CEDULA            varchar(10),
   DP_TELEFONO          varchar(10),
   DP_CELULAR           varchar(10),
   DP_CORREO            varchar(50),
   DP_LATITUD           numeric(8,0),
   DP_LONGUITUD         numeric(8,0),
   DP_PRESENTACION      varchar(500),
   DP_PREFERENCIASALARIAL numeric(8,0),
   DP_FOTO              varchar(50),
   primary key (DP_ID)
);

/*==============================================================*/
/* Table: DETALLE_CATALOGO                                      */
/*==============================================================*/
create table DETALLE_CATALOGO
(
   DC_ID                numeric(8,0) not null,
   DCT_ID               numeric(8,0),
   DC_DESCRIPCION       varchar(50),
   primary key (DC_ID)
);

/*==============================================================*/
/* Table: DETALLE_CATALOGO_TIPO                                 */
/*==============================================================*/
create table DETALLE_CATALOGO_TIPO
(
   DCT_ID               numeric(8,0) not null,
   DCT_DESCRIPCION      varchar(50),
   primary key (DCT_ID)
);

/*==============================================================*/
/* Table: EMPRESA                                               */
/*==============================================================*/
create table EMPRESA
(
   EMP_ID               numeric(8,0) not null,
   DE_ID                numeric(8,0),
   primary key (EMP_ID)
);

/*==============================================================*/
/* Table: ESTUDIOS                                              */
/*==============================================================*/
create table ESTUDIOS
(
   ES_ID                numeric(8,0) not null,
   POS_ID               numeric(8,0),
   INSTITUCION          numeric(8,0),
   NIVEL                numeric(8,0),
   ESTADO               numeric(8,0),
   PA_ID                numeric(8,0),
   AR_ID                numeric(8,0),
   ES_TITULO            varchar(50),
   ES_FEHCA_INICIO      date,
   ES_FECHA_FIN         date,
   ES_PORCENTAJE        numeric(8,0),
   primary key (ES_ID)
);

/*==============================================================*/
/* Table: EXPERIENCIA_LABORAL                                   */
/*==============================================================*/
create table EXPERIENCIA_LABORAL
(
   EL_ID                numeric(8,0) not null,
   SENORITY             numeric(8,0),
   SA_ID                numeric(8,0),
   POS_ID               numeric(8,0),
   INDUSTRIA            numeric(8,0),
   AR_ID                numeric(8,0),
   PA_ID                numeric(8,0),
   EL_TITULO            varchar(50),
   EL_FECHA_INICIO      date,
   EL_EMPRESA           varchar(50),
   EL_FECHA_FIN         date,
   EL_DESCRIPCION       varchar(50),
   primary key (EL_ID)
);

/*==============================================================*/
/* Table: IDIOMAS                                               */
/*==============================================================*/
create table IDIOMAS
(
   ID_ID                numeric(8,0) not null,
   IDIOMA               numeric(8,0),
   POS_ID               numeric(8,0),
   ESCRITO_NIVEL        numeric(8,0),
   ORAL_NIVEL           numeric(8,0),
   primary key (ID_ID)
);

/*==============================================================*/
/* Table: INFORMATICA                                           */
/*==============================================================*/
create table INFORMATICA
(
   IN_ID                numeric(8,0) not null,
   NIVEL                numeric(8,0),
   POS_ID               numeric(8,0),
   SA_ID                numeric(8,0),
   AR_ID                numeric(8,0),
   primary key (IN_ID)
);

/*==============================================================*/
/* Table: PAIS                                                  */
/*==============================================================*/
create table PAIS
(
   PA_ID                numeric(8,0) not null,
   PA_DESCRIPCION       varchar(50),
   primary key (PA_ID)
);

/*==============================================================*/
/* Table: POSTULANTE                                            */
/*==============================================================*/
create table POSTULANTE
(
   POS_ID               numeric(8,0) not null,
   DP_ID                numeric(8,0),
   primary key (POS_ID)
);

/*==============================================================*/
/* Table: PROVINCIA                                             */
/*==============================================================*/
create table PROVINCIA
(
   PRO_ID               numeric(8,0) not null,
   PA_ID                numeric(8,0),
   PRO_DESCRIPCION      varchar(50),
   primary key (PRO_ID)
);

/*==============================================================*/
/* Table: REFERENCIAS                                           */
/*==============================================================*/
create table REFERENCIAS
(
   RE_ID                numeric(8,0) not null,
   POS_ID               numeric(8,0),
   TIPO                 numeric(8,0),
   EL_ID                numeric(8,0),
   RELACION             numeric(8,0),
   RE_NOMBRE            varchar(50),
   RE_APELLIDO          varchar(50),
   RE_CORREO            varchar(50),
   RE_TELEFONO          varchar(10),
   RE_PUESTO            varchar(50),
   primary key (RE_ID)
);

/*==============================================================*/
/* Table: REGISTRO                                              */
/*==============================================================*/
create table REGISTRO
(
   REG_ID               numeric(8,0) not null,
   POS_ID               numeric(8,0),
   AV_ID                numeric(8,0),
   primary key (REG_ID)
);

/*==============================================================*/
/* Table: ROLE                                                  */
/*==============================================================*/
create table ROLE
(
   ROLE_ID              numeric(8,0) not null,
   ROLE                 varchar(50) not null,
   primary key (ROLE_ID)
);

/*==============================================================*/
/* Table: SUBAREA                                               */
/*==============================================================*/
create table SUBAREA
(
   SA_ID                numeric(8,0) not null,
   AR_ID                numeric(8,0),
   SA_DESCRIPCION       varchar(50),
   primary key (SA_ID)
);

/*==============================================================*/
/* Table: USER                                                  */
/*==============================================================*/
create table USER
(
   USERNAME             varchar(50) not null,
   PASSWORD             varchar(50) not null,
   primary key (USERNAME)
);

/*==============================================================*/
/* Table: USER_ROLE                                             */
/*==============================================================*/
create table USER_ROLE
(
   USERROLE_ID          numeric(8,0) not null,
   USERNAME             varchar(50),
   POS_ID               numeric(8,0),
   ROLE_ID              numeric(8,0),
   EMP_ID               numeric(8,0),
   primary key (USERROLE_ID)
);

alter table AVISO add constraint FK_FK_AREA_AVISO foreign key (AR_ID)
      references AREA (AR_ID) on delete restrict on update restrict;

alter table AVISO add constraint FK_FK_EMPRESA_AVISO foreign key (EMP_ID)
      references EMPRESA (EMP_ID) on delete restrict on update restrict;

alter table AVISO add constraint FK_FK_PROVINCIA_AVISO foreign key (PRO_ID)
      references PROVINCIA (PRO_ID) on delete restrict on update restrict;

alter table AVISO add constraint FK_FK_SUBAREA_AVISO foreign key (SA_ID)
      references SUBAREA (SA_ID) on delete restrict on update restrict;

alter table AVISO add constraint FK_FK_TIPO_PUESTO_AVISO foreign key (DC_ID)
      references DETALLE_CATALOGO (DC_ID) on delete restrict on update restrict;

alter table CONOCIMIENTO_ADICIONAL add constraint FK_FK_CONOCIMIENTO_POSTULANTE foreign key (POS_ID)
      references POSTULANTE (POS_ID) on delete restrict on update restrict;

alter table DETALLE_CATALOGO add constraint FK_FK_DETALLE_CATALOGO_TIPO foreign key (DCT_ID)
      references DETALLE_CATALOGO_TIPO (DCT_ID) on delete restrict on update restrict;

alter table EMPRESA add constraint FK_FK_DATOS_EMPRESA foreign key (DE_ID)
      references DATOS_EMPRESA (DE_ID) on delete restrict on update restrict;

alter table ESTUDIOS add constraint FK_FK_AREA_ESTUDIOS foreign key (AR_ID)
      references AREA (AR_ID) on delete restrict on update restrict;

alter table ESTUDIOS add constraint FK_FK_ESTADO foreign key (INSTITUCION)
      references DETALLE_CATALOGO (DC_ID) on delete restrict on update restrict;

alter table ESTUDIOS add constraint FK_FK_INSTITUCION foreign key (ESTADO)
      references DETALLE_CATALOGO (DC_ID) on delete restrict on update restrict;

alter table ESTUDIOS add constraint FK_FK_NIVEL foreign key (NIVEL)
      references DETALLE_CATALOGO (DC_ID) on delete restrict on update restrict;

alter table ESTUDIOS add constraint FK_FK_PAIS_ESTUDIOS foreign key (PA_ID)
      references PAIS (PA_ID) on delete restrict on update restrict;

alter table ESTUDIOS add constraint FK_FK_POSTULANTE_ESTUDIOS foreign key (POS_ID)
      references POSTULANTE (POS_ID) on delete restrict on update restrict;

alter table EXPERIENCIA_LABORAL add constraint FK_FK_AREA foreign key (AR_ID)
      references AREA (AR_ID) on delete restrict on update restrict;

alter table EXPERIENCIA_LABORAL add constraint FK_FK_INDUSTRIA foreign key (INDUSTRIA)
      references DETALLE_CATALOGO (DC_ID) on delete restrict on update restrict;

alter table EXPERIENCIA_LABORAL add constraint FK_FK_PAIS foreign key (PA_ID)
      references PAIS (PA_ID) on delete restrict on update restrict;

alter table EXPERIENCIA_LABORAL add constraint FK_FK_POSTULANTE_EXPERIENCIA foreign key (POS_ID)
      references POSTULANTE (POS_ID) on delete restrict on update restrict;

alter table EXPERIENCIA_LABORAL add constraint FK_FK_SENIORITY foreign key (SENORITY)
      references DETALLE_CATALOGO (DC_ID) on delete restrict on update restrict;

alter table EXPERIENCIA_LABORAL add constraint FK_FK_SUBAREA foreign key (SA_ID)
      references SUBAREA (SA_ID) on delete restrict on update restrict;

alter table IDIOMAS add constraint FK_FK_IDIOMA_DESCRIPCION foreign key (ORAL_NIVEL)
      references DETALLE_CATALOGO (DC_ID) on delete restrict on update restrict;

alter table IDIOMAS add constraint FK_FK_NIVEL_ESCRITO foreign key (ESCRITO_NIVEL)
      references DETALLE_CATALOGO (DC_ID) on delete restrict on update restrict;

alter table IDIOMAS add constraint FK_FK_NIVEL_ORAL foreign key (IDIOMA)
      references DETALLE_CATALOGO (DC_ID) on delete restrict on update restrict;

alter table IDIOMAS add constraint FK_FK_POSTULANTE_IDIOMAS foreign key (POS_ID)
      references POSTULANTE (POS_ID) on delete restrict on update restrict;

alter table INFORMATICA add constraint FK_FK_AREA_INFORMATICA foreign key (AR_ID)
      references AREA (AR_ID) on delete restrict on update restrict;

alter table INFORMATICA add constraint FK_FK_NIVEL_INFORMATICA foreign key (NIVEL)
      references DETALLE_CATALOGO (DC_ID) on delete restrict on update restrict;

alter table INFORMATICA add constraint FK_FK_POSTULANTE_INFORMATICA foreign key (POS_ID)
      references POSTULANTE (POS_ID) on delete restrict on update restrict;

alter table INFORMATICA add constraint FK_FK_SUBAREA_INFORMATICA foreign key (SA_ID)
      references SUBAREA (SA_ID) on delete restrict on update restrict;

alter table POSTULANTE add constraint FK_FK_DATOS_POSTULANTE foreign key (DP_ID)
      references DATOS_PERSONALES (DP_ID) on delete restrict on update restrict;

alter table PROVINCIA add constraint FK_FK_PAIS_PROVINCIA foreign key (PA_ID)
      references PAIS (PA_ID) on delete restrict on update restrict;

alter table REFERENCIAS add constraint FK_FK_EXPERIENCIA_REFERENCIAS foreign key (EL_ID)
      references EXPERIENCIA_LABORAL (EL_ID) on delete restrict on update restrict;

alter table REFERENCIAS add constraint FK_FK_POSTULANTE_REFERENCIAS foreign key (POS_ID)
      references POSTULANTE (POS_ID) on delete restrict on update restrict;

alter table REFERENCIAS add constraint FK_FK_RELACION_REFERENCIAS foreign key (TIPO)
      references DETALLE_CATALOGO (DC_ID) on delete restrict on update restrict;

alter table REFERENCIAS add constraint FK_FK_TIPO_REFERENCIAS foreign key (RELACION)
      references DETALLE_CATALOGO (DC_ID) on delete restrict on update restrict;

alter table REGISTRO add constraint FK_POSTULANTE_REGISTRO foreign key (POS_ID)
      references POSTULANTE (POS_ID) on delete restrict on update restrict;

alter table REGISTRO add constraint FK_AVISO_REGISTRO foreign key (AV_ID)
      references AVISO (AV_ID) on delete restrict on update restrict;
	  
	  
alter table SUBAREA add constraint FK_FK_AREA_SUBAREA foreign key (AR_ID)
      references AREA (AR_ID) on delete restrict on update restrict;

alter table USER_ROLE add constraint FK_FK_EMPRESA_USER_ROLE foreign key (EMP_ID)
      references EMPRESA (EMP_ID) on delete restrict on update restrict;

alter table USER_ROLE add constraint FK_FK_POSTULANTE_USER_ROLE foreign key (POS_ID)
      references POSTULANTE (POS_ID) on delete restrict on update restrict;

alter table USER_ROLE add constraint FK_FK_ROLE foreign key (ROLE_ID)
      references ROLE (ROLE_ID) on delete restrict on update restrict;

alter table USER_ROLE add constraint FK_FK_USER foreign key (USERNAME)
      references USER (USERNAME) on delete restrict on update restrict;


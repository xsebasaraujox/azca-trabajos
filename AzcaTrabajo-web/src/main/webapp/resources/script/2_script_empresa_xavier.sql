ALTER TABLE `azcatrabajos`.`aviso` ADD COLUMN `AV_TITULO` VARCHAR(100) NULL DEFAULT NULL  AFTER `AV_DESCRIPCION` ;
ALTER TABLE `azcatrabajos`.`aviso` ADD COLUMN `AV_DISCAPACITADO` BIT NULL DEFAULT NULL  AFTER `AV_TITULO` ;
ALTER TABLE `azcatrabajos`.`aviso` ADD COLUMN `AV_TIPO_PUESTO` VARCHAR(50) NULL DEFAULT NULL  AFTER `AV_DISCAPACITADO` ;
create table PREGUNTAS(
	PRG_ID numeric(8,0) not null,
	AV_ID numeric(8,0),
	PRG_TITULO varchar(50),
	PRG_RESPUESTA varchar(100) null,
	primary key (PRG_ID)	
);
alter table PREGUNTAS add constraint FK_FK_AVISO_PREGUNTAS foreign key (AV_ID)
      references AVISO (AV_ID) on delete restrict on update restrict;

INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('1', 'Abastecimiento y Logística');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('2', 'Administración, Contabilidad y Finanzas');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('3', 'Aduana y Comercio Exterior');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('4', 'Atención al Cliente, Call Center y Telemarketing');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('5', 'Comercial, Ventas y Negocios');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('6', 'Comunicación, Relaciones Institucionales y Pública');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('7', 'Diseño');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('8', 'Educación, Docencia e Investigación');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('9', 'Gastronomía y Turismo');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('10', 'Gerencia y Dirección General');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('11', 'Ingeniería Civil y Construcción');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('12', 'Ingenierías');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('13', 'Legales');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('14', 'Marketing y Publicidad');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('15', 'Minería, Petróleo y Gas');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('16', 'Oficios y Otros');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('17', 'Producción y Manufactura');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('18', 'Recursos Humanos y Capacitación');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('19', 'Salud, Medicina y Farmacia');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('20', 'Secretarias y Recepción');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('21', 'Seguros');
INSERT INTO `azcatrabajos`.`area` (`AR_ID`, `AR_DESCRIPCION`) VALUES ('22', 'Tecnología, Sistemas y Telecomunicaciones');

INSERT INTO `azcatrabajos`.`subarea` (`SA_ID`, `AR_ID`, `SA_DESCRIPCION`) VALUES ('1', '1', 'Abastecimiento');
INSERT INTO `azcatrabajos`.`subarea` (`SA_ID`, `AR_ID`, `SA_DESCRIPCION`) VALUES ('2', '1', 'Almacén/Depósito/Expedición');
INSERT INTO `azcatrabajos`.`subarea` (`SA_ID`, `AR_ID`, `SA_DESCRIPCION`) VALUES ('3', '1', 'Asistente de Tráfico');
INSERT INTO `azcatrabajos`.`subarea` (`SA_ID`, `AR_ID`, `SA_DESCRIPCION`) VALUES ('4', '1', 'Compras');
INSERT INTO `azcatrabajos`.`subarea` (`SA_ID`, `AR_ID`, `SA_DESCRIPCION`) VALUES ('5', '1', 'Distribución');
INSERT INTO `azcatrabajos`.`subarea` (`SA_ID`, `AR_ID`, `SA_DESCRIPCION`) VALUES ('6', '1', 'Logística');
INSERT INTO `azcatrabajos`.`subarea` (`SA_ID`, `AR_ID`, `SA_DESCRIPCION`) VALUES ('7', '1', 'Transport');

ALTER TABLE `azcatrabajos`.`aviso` CHANGE COLUMN `AV_SALARIO` `AV_SALARIO` VARCHAR(20) NULL DEFAULT NULL  ;

INSERT INTO `azcatrabajos`.`pais` (`PA_ID`, `PA_DESCRIPCION`) VALUES ('1', 'ECUADOR');

INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('1', '1', 'ESMERALDAS');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('2', '1', 'MANABI');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('3', '1', 'LOS RIOS');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('4', '1', 'GUAYAS');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('5', '1', 'SANTA ELENA');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('6', '1', 'CARCHI');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('7', '1', 'IMBABURA');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('8', '1', 'PICHINCHA');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('9', '1', 'SANTO DOMINGO');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('10', '1', 'COTOPAXI');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('11', '1', 'TUNGURAHUA');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('12', '1', 'CHIMBORAZO');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('13', '1', 'BOLIVAR');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('14', '1', 'CAÑAR');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('15', '1', 'AZUAY');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('16', '1', 'LOJA');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('17', '1', 'SUCUMBIOS');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('18', '1', 'ORELLANA');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('19', '1', 'NAPO');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('20', '1', 'PASTAZA');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('21', '1', 'MORONA SANTIAGO');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('22', '1', 'ZAMORA CHINCHIPE');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('23', '1', 'GALAPAGOS');
INSERT INTO `azcatrabajos`.`provincia` (`PRO_ID`, `PA_ID`, `PRO_DESCRIPCION`) VALUES ('24', '1', 'EL ORO');

INSERT INTO `azcatrabajos`.`detalle_catalogo_tipo` (`DCT_ID`, `DCT_DESCRIPCION`) VALUES ('1', 'DIAS DE LA SEMANA');

INSERT INTO `azcatrabajos`.`detalle_catalogo` (`DC_ID`, `DCT_ID`, `DC_DESCRIPCION`) VALUES ('1', '1', 'LUNES');
INSERT INTO `azcatrabajos`.`detalle_catalogo` (`DC_ID`, `DCT_ID`, `DC_DESCRIPCION`) VALUES ('2', '1', 'MARTES');
INSERT INTO `azcatrabajos`.`detalle_catalogo` (`DC_ID`, `DCT_ID`, `DC_DESCRIPCION`) VALUES ('3', '1', 'MIERCOLES');
INSERT INTO `azcatrabajos`.`detalle_catalogo` (`DC_ID`, `DCT_ID`, `DC_DESCRIPCION`) VALUES ('4', '1', 'JUEVES');
INSERT INTO `azcatrabajos`.`detalle_catalogo` (`DC_ID`, `DCT_ID`, `DC_DESCRIPCION`) VALUES ('5', '1', 'VIERNES');
INSERT INTO `azcatrabajos`.`detalle_catalogo` (`DC_ID`, `DCT_ID`, `DC_DESCRIPCION`) VALUES ('6', '1', 'SABADO');
INSERT INTO `azcatrabajos`.`detalle_catalogo` (`DC_ID`, `DCT_ID`, `DC_DESCRIPCION`) VALUES ('7', '1', 'DOMINGO');





